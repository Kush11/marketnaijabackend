﻿using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class WalletRepository : _dataRepository<Wallet>
    {
        readonly StoresContext _context;

        public WalletRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task Add(Wallet entity)
        {
            await _context.wallet.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Wallet wallet)
        {
            _context.wallet.Remove(wallet);
            await _context.SaveChangesAsync();
        }

        public async Task<Wallet> Get(Guid id)
        {
            return await _context.wallet

                  .FirstOrDefaultAsync(c => c.StoreId == id);
        }

        public async Task<IEnumerable<Wallet>> GetAll()
        {
            return await _context.wallet

                .ToListAsync();
        }

        public IEnumerable<Wallet> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Wallet> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Wallet wallet, Wallet entity)
        {
            wallet.Balance = entity.Balance;
            await _context.SaveChangesAsync();
        }

    }
}

