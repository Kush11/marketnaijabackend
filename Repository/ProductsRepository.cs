﻿using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Repository
{
    public class ProductsRepository : _dataRepository<Products>
    {
        readonly StoresContext _context;

        public ProductsRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task Add(Products entity)
        {
            await _context.Products.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Products products)
        {
            _context.Products.Remove(products);
            await _context.SaveChangesAsync();
        }

        public async Task<Products> Get(Guid id)
        {
            return await _context.Products
                .Include(s => s.Store)
                .Include(f => f.file)
                .FirstOrDefaultAsync(c => c.ProductId == id);
        }


        public async Task<IEnumerable<Products>> GetAll()
        {
            return await _context.Products
                .Include(s => s.Store)
                .Include(f => f.file)
                .ToListAsync();
        }

        public IEnumerable<Products> GetAllProducts()
        {
            return _context.Products
                .Include(f => f.file)
                .ToList();
        }

        public IEnumerable<Products> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Products products, Products entity)
        {
            products.ProductName = entity.ProductName;
            products.Price = entity.Price;
            products.Discount = entity.Discount;
            //products.Description = entity.Description;
            await _context.SaveChangesAsync();
        }
    }
}
