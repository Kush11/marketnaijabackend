﻿using marketNaija.Data;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class AppointmentSessionRepository : _dataRepository<AppointmentSession>
    {
        readonly StoresContext _context;

        public AppointmentSessionRepository(StoresContext context)
        {
            _context = context;
        }

        public async Task Add(AppointmentSession entity)
        {
            await _context.AppointmentSessions.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(AppointmentSession entity)
        {
            _context.AppointmentSessions.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<AppointmentSession> Get(Guid id)
        {
            return await _context.AppointmentSessions
                .Include(p => p.Patient)
                .ThenInclude(u => u.User)
                .Include(pr => pr.Prescriptions)
                .FirstOrDefaultAsync(c => c.SessionId == id || c.Patient.UserId == id);
        }

        public async Task<IEnumerable<AppointmentSession>> GetAll()
        {
            return await _context.AppointmentSessions
                .Include(p => p.Patient)
                .ThenInclude(u => u.User)
                .Include(pr => pr.Prescriptions)
                .ToListAsync();

        }

        public IEnumerable<AppointmentSession> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AppointmentSession> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(AppointmentSession dbEntity, AppointmentSession entity)
        {
            dbEntity.Status = entity.Status;
            dbEntity.SessionEndDateTime = entity.SessionEndDateTime;
            dbEntity.SessionStartDateTime = entity.SessionStartDateTime;
            dbEntity.Diagnosis = entity.Diagnosis;
            dbEntity.IsPaymentDone = entity.IsPaymentDone;
            await _context.SaveChangesAsync();
        }
    }
}
