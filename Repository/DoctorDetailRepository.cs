﻿using marketNaija.Data;
using marketNaija.Helpers;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace MarketNaijaApp.Repository
{
    public class DoctorDetailRepository : _dataRepository<DoctorDetails>
    {
        readonly StoresContext _context;
        private readonly AppSettings _appSettings;


        public DoctorDetailRepository(StoresContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;

        }

        public async Task<IEnumerable<DoctorDetails>> GetAll()
        {
            return await _context.DoctorDetails
                 .Include(u => u.User)
                 .Include(s => s.Specialization)
                 .ToListAsync();
        }

        public async Task<DoctorDetails> Get(Guid id)
        {
          
            return await _context.DoctorDetails
                 .Include(u => u.User)
                 .Include(s => s.Specialization)
                 //.Include(a => a.)
                 //.TakeWhile(c => c.DoctorId == id || c.UserId == id)
                 .FirstOrDefaultAsync(c => c.DoctorId == id || c.UserId == id);
        }

        public async Task Add(DoctorDetails entity)
        {
            await _context.DoctorDetails.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(DoctorDetails doctor, DoctorDetails entity)
        {
            doctor.IsAvailable = entity.IsAvailable;
            await _context.SaveChangesAsync();
        }

        public async Task Delete(DoctorDetails doctor)
        {
            _context.DoctorDetails.Remove(doctor);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<DoctorDetails> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DoctorDetails> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public string GetTurnToken()
        {
            string accountSid = "AC8a255ea7f6d7383d902caea9c0ce04b6";
            string authToken = "9d1f885ff310bdf29fa37e7e99c37022";

            TwilioClient.Init(accountSid, authToken);

            var token = TokenResource.Create();
            return token.Username;

        }
    }
}
