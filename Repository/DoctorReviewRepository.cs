﻿using marketNaija.Data;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PharmaHub.Repository
{
    public class DoctorReviewRepository : _dataRepository<DoctorReview>
    {

        readonly StoresContext _context;

        public DoctorReviewRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task Add(DoctorReview entity)
        {
            await _context.DoctorReviews.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(DoctorReview entity)
        {
            _context.DoctorReviews.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<DoctorReview> Get(Guid id)
        {
            return await _context.DoctorReviews
                    .Include(cs => cs.DoctorDetails)
                    .FirstOrDefaultAsync(c => c.DoctorId == id);
        }

        public async Task<IEnumerable<DoctorReview>> GetAll()
        {
            return await _context.DoctorReviews
                    .Include(c => c.DoctorDetails)
                    .ToListAsync();
        }

        public IEnumerable<DoctorReview> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DoctorReview> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(DoctorReview dbEntity, DoctorReview entity)
        {
            dbEntity.Comment = entity.Comment;
            dbEntity.UserRate = entity.UserRate;
            await _context.SaveChangesAsync();
        }
    }
}
