﻿using marketNaija.Data;
using marketNaija.Models;
using MarketNaijaApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class StoreImageRepository : _imageRepository<StoreImage>
    {
        readonly StoresContext _context;
        public StoreImageRepository(StoresContext context)
        {
            _context = context;
        }

        public void Add(StoreImage entity)
        {
            _context.StoreImage.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(StoreImage Files)
        {
            _context.StoreImage.Remove(Files);
            _context.SaveChanges();
        }

        public StoreImage Get(Guid id)
        {
            return _context.StoreImage
                .FirstOrDefault(c => c.StoreId == id);
        }

        public IEnumerable<StoreImage> GetAll()
        {
            return _context.StoreImage
                .ToList();
        }

        public void Update(StoreImage Files, StoreImage entity)
        {
            Files.File = entity.File;
            _context.SaveChanges();
        }
    }
}

