﻿using marketNaija.Data;
using marketNaija.Helpers;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class ProductsAdsRepository : _dataRepository<ProductAds>
    {
        readonly StoresContext _context;

        public ProductsAdsRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task Add(ProductAds entity)
        {
            await _context.ProductAds.AddAsync(entity);
            if (_context.ProductAds.Any(x => x.ProductId == entity.ProductId))
                throw new AppException("An Ad is already running for this Product");
            await _context.SaveChangesAsync();
        }

        public async Task Delete(ProductAds productAds)
        {
            _context.ProductAds.Remove(productAds);
            await _context.SaveChangesAsync();
        }

        public async Task<ProductAds> Get(Guid id)
        {
            return await _context.ProductAds
                .Include(p => p.PromoteAds)
                .Include(cs => cs.Products)
                .ThenInclude(f => f.file)
                .FirstOrDefaultAsync(c => c.AdsId == id);
        }
        //public IQueryable<Stores> Gettores(int id)
        //{
        //    return _context.Stores
        //        .Where(u => u.UserId == id);
        //}
        public async Task<IEnumerable<ProductAds>> GetAll()
        {
            return await _context.ProductAds
                .Include(p => p.PromoteAds)
                .Include(cs => cs.Products)
                .ThenInclude(f => f.file)
                .ToListAsync();
        }

        public IEnumerable<ProductAds> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductAds> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(ProductAds productAds, ProductAds entity)
        {
            productAds.Status = entity.Status;
            await _context.SaveChangesAsync();
        }

        //public Stores GetStoreByUserId(int id)
        //{
        //    return _context.Stores
        //        .Where(u => u.UserId == id)
        //        .Include(us => us.User)
        //        .FirstOrDefault(c => c.StoreId == id);

        //}
    }
}
