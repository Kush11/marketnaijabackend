﻿using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public interface ITypedHubClient
    {
        Task ReceiveMessage(string message);
        Task GetConnectionId(string id);
        Task AddToGroup(string groupName);
        Task ReceiveGroupMessage(string message);
        Task CallGroupMember(string message);
        Task SendSignal(string signal);
        Task EndGroupCall(string message);
        Task ReceiveSignal(string signal);

    }
}