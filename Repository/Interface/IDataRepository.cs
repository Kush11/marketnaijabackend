﻿using marketNaija.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace marketNaija.Repository.Interface
{
    
    public interface _dataRepository<TEntity>
    {
        Task <IEnumerable<TEntity>> GetAll();

        IEnumerable<TEntity> GetAllStores();

        IEnumerable<TEntity> GetAllProducts();
       
        Task <TEntity> Get(Guid id);
        Task Add(TEntity entity);
        Task Update(TEntity dbEntity, TEntity entity);
        Task Delete(TEntity entity);
       


    }
}
