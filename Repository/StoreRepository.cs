﻿using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class StoreRepository : _dataRepository<Stores>
    {
        readonly StoresContext _context;

        public StoreRepository(StoresContext context)
        {
            _context = context;
        }

        public async Task Add(Stores entity)
        {
            await _context.Stores.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Stores stores)
        {
            _context.Stores.Remove(stores);
            await _context.SaveChangesAsync();
        }

        public async Task<Stores> Get(Guid id)
        {
            return await _context.Stores
                .Include(s => s.Products)
                .ThenInclude(f => f.file)
                 .Include(f => f.StoreImage)
                 .Include(o => o.Orders)
                 .Include(r => r.Review)
                 .Include(sa => sa.StoreAds)
                 .ThenInclude(sp => sp.PromoteAds)
                 .Include(w => w.Wallet)
                 .Include(u => u.User)
                .FirstOrDefaultAsync(c => c.StoreId == id);
        }

        public async Task<IEnumerable<Stores>> GetAll()
        {
            return await _context.Stores
                 .Include(f => f.StoreImage)
                 .Include(r => r.Review)
                 .Include(u => u.User)
                .ToListAsync();
        }

        public IEnumerable<Stores> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Stores> GetAllStores()
        {
            return _context.Stores
                 .Include(f => f.StoreImage)
                .ToList();
        }
        public async Task Update(Stores stores, Stores entity)
        {
            stores.StoreName = entity.StoreName;
            stores.Description = entity.Description;
            stores.Address = entity.Address;
            stores.PhoneNumber = entity.PhoneNumber;
            stores.ConfirmStatus = entity.ConfirmStatus;
            await _context.SaveChangesAsync();
        }


    }
}

