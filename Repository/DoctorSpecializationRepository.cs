﻿using marketNaija.Data;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class DoctorSpecializationRepository : _dataRepository<DoctorSpecialization>
    {
        readonly StoresContext _context;

        public DoctorSpecializationRepository(StoresContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<DoctorSpecialization>> GetAll()
        {
            return await _context.DoctorSpecialization
                .ToListAsync();
        }

        public async Task<DoctorSpecialization> Get(Guid id)
        {
            return await _context.DoctorSpecialization
            
                 .FirstOrDefaultAsync(c => c.SpecializationId == id);
        }

        public async Task Add(DoctorSpecialization entity)
        {
            await _context.DoctorSpecialization.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(DoctorSpecialization doctorSpecialization, DoctorSpecialization entity)
        {
            doctorSpecialization.SpecializationName = entity.SpecializationName;
            await _context.SaveChangesAsync();
        }

        public async Task Delete(DoctorSpecialization doctor)
        {
            _context.DoctorSpecialization.Remove(doctor);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<DoctorSpecialization> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DoctorSpecialization> GetAllProducts()
        {
            throw new NotImplementedException();
        }
    }
}
