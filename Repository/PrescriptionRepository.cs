﻿using marketNaija.Data;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class PrescriptionRepository : _dataRepository<Prescription>
    {

        readonly StoresContext _context;

        public PrescriptionRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task Add(Prescription entity)
        {
            await _context.Prescriptions.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Prescription entity)
        {
            _context.Prescriptions.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<Prescription> Get(Guid id)
        {
            return await _context.Prescriptions
                .FirstOrDefaultAsync(c => c.SessionId == id);
        }

        public async Task<IEnumerable<Prescription>> GetAll()
        {
            return await _context.Prescriptions
                .ToListAsync();
        }

        public IEnumerable<Prescription> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Prescription> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Prescription dbEntity, Prescription entity)
        {
            dbEntity.Comment = entity.Comment;
            await _context.SaveChangesAsync();
        }
    }
}
