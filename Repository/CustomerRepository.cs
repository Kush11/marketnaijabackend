﻿using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Models.DTO;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Repository
{
    public class CustomerRepository : _dataRepository<Customers>
    {
        readonly StoresContext _context;

        public CustomerRepository(StoresContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Customers>> GetAll()
        {
            return await _context.Customers
                 .Include(u => u.User)
                .ToListAsync();
        }

        public async Task<Customers> Get(Guid id)
        {
            return await _context.Customers
                 .Include(u => u.User)
                 .FirstOrDefaultAsync(c => c.CustomerId == id);
        }

        public async Task Add(Customers entity)
        {
           await _context.Customers.AddAsync(entity);
           await _context.SaveChangesAsync();
        }

        public async Task Update(Customers customer, Customers entity)
        {
            customer.UserName = entity.UserName;
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Customers customer)
        {
            _context.Customers.Remove(customer);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Customers> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Customers> GetAllProducts()
        {
            throw new NotImplementedException();
        }
    }
}
