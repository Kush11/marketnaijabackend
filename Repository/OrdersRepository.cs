﻿using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Models.DTO;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Repository
{
    public class OrdersRepository : _dataRepository<Orders>
    {
        readonly StoresContext _context;

        public OrdersRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task Add(Orders entity)
        {
            await _context.Orders.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Orders orders)
        {
            _context.Orders.Remove(orders);
            await _context.SaveChangesAsync();
        }

        public async Task<Orders> Get(Guid id)
        {
            return await _context.Orders
                
            .FirstOrDefaultAsync(c => c.OrderId == id);
        }

        public async Task<IEnumerable<Orders>> GetAll()
        {
            return await _context.Orders              
            .ToListAsync();
        }

        public IEnumerable<Orders> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Orders> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Orders orders, Orders entity)
        {
            orders.OrderStatus = entity.OrderStatus;
            await _context.SaveChangesAsync();
        }

    }
}

