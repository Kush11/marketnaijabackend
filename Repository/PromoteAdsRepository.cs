﻿using marketNaija.Data;
using marketNaija.Helpers;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class PromoteAdsRepository : _dataRepository<PromoteAds>
    {
        readonly StoresContext _context;

        public PromoteAdsRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task  Add(PromoteAds entity)
        {
            await _context.PromoteAds.AddAsync(entity);
            if (_context.PromoteAds.Any(x => x.PromotionId == entity.PromotionId))
                throw new AppException("This Promotion is already running for this Product");
            await _context.SaveChangesAsync();
        }

        public async Task Delete(PromoteAds promoteAds)
        {
            _context.PromoteAds.Remove(promoteAds);
            await _context.SaveChangesAsync();
        }

        public async Task<PromoteAds> Get(Guid id)
        {
            return await _context.PromoteAds
                   .FirstOrDefaultAsync(c => c.PromotionId == id);
        }
        //public IQueryable<Stores> Gettores(int id)
        //{
        //    return _context.Stores
        //        .Where(u => u.UserId == id);
        //}
        public async Task<IEnumerable<PromoteAds>> GetAll()
        {
            return await _context.PromoteAds
                .ToListAsync();
        }

        public IEnumerable<PromoteAds> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PromoteAds> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(PromoteAds promoteAds, PromoteAds entity)
        {
            promoteAds.PackageName = entity.PackageName;
            promoteAds.price = entity.price;
            promoteAds.duration = entity.duration;
            await _context.SaveChangesAsync();
        }
    }
}
