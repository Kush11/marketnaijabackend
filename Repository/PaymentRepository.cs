﻿using marketNaija.Data;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using PharmaHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PharmaHub.Repository
{
    public class PaymentRepository : _dataRepository<PaymentData>
    {
        readonly StoresContext _context;

        public PaymentRepository(StoresContext context)
        {
            _context = context;
        }

        public async Task Add(PaymentData payment)
        {
            await _context.PaymentData.AddAsync(payment);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(PaymentData entity)
        {
            _context.PaymentData.Remove(entity);
            await _context.SaveChangesAsync();

        }

        public async Task<PaymentData> Get(Guid id)
        {
            return await _context.PaymentData
                .FirstOrDefaultAsync(c => c.PaymentId == id);
        }

        public async Task<IEnumerable<PaymentData>> GetAll()
        {
            return await _context.PaymentData
                .ToListAsync();
        }

        public IEnumerable<PaymentData> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PaymentData> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(PaymentData payment, PaymentData entity)
        {
            payment.PaymentToken = entity.PaymentToken;
            await _context.SaveChangesAsync();

        }



    }
}
