﻿using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class ReviewRepository : _dataRepository<Review>
    {
        readonly StoresContext _context;

        public ReviewRepository(StoresContext context)
        {
            _context = context;
        }

        public async Task Add(Review entity)
        {
            await _context.Reviews.AddAsync(entity);
            await _context.SaveChangesAsync();
        }
      
        public async Task Delete(Review review)
        {
            _context.Reviews.Remove(review);
            await _context.SaveChangesAsync();
        }
        public async Task<Review> Get(Guid id)
        {
            return await _context.Reviews
                   .Include(cs => cs.Store)
                   .FirstOrDefaultAsync(c => c.StoreId == id);
        }

        public async Task<IEnumerable<Review>> GetAll()
        {
            return await _context.Reviews
                .Include(c => c.Store)
                .ToListAsync();
        }

        public IEnumerable<Review> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Review> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Review review, Review entity)
        {
            review.Comment = entity.Comment;
            review.UserRate = entity.UserRate;
            await _context.SaveChangesAsync();
        }
    }

  }
