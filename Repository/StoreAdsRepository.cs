﻿using marketNaija.Data;
using marketNaija.Helpers;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Repository
{
    public class StoreAdsRepository : _dataRepository<StoreAds>
    {
        readonly StoresContext _context;

        public StoreAdsRepository(StoresContext context)
        {
            _context = context;
        }


        public async Task Add(StoreAds entity)
        {
            await _context.StoreAds.AddAsync(entity);
            if (_context.StoreAds.Any(x => x.StoreId == entity.StoreId))
                throw new AppException("An Ad is already running for this store");
            await _context.SaveChangesAsync();
        }

        public async Task Delete(StoreAds storeAds)
        {
            _context.StoreAds.Remove(storeAds);
            await _context.SaveChangesAsync();
        }

        public async Task<StoreAds> Get(Guid id)
        {
            return await _context.StoreAds
                   .Include(p => p.PromoteAds)
                   .Include(cs => cs.Stores)
                   .ThenInclude(f => f.StoreImage)
                   .FirstOrDefaultAsync(c => c.AdsId == id);
        }
        //public IQueryable<Stores> Gettores(int id)
        //{
        //    return _context.Stores
        //        .Where(u => u.UserId == id);
        //}
        public async Task<IEnumerable<StoreAds>> GetAll()
        {
            return await _context.StoreAds
                .Include(p => p.PromoteAds)
                .Include(cs => cs.Stores)
                .ThenInclude(f => f.StoreImage)
                .ToListAsync();
        }

        public IEnumerable<StoreAds> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<StoreAds> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(StoreAds storeAds, StoreAds entity)
        {
            storeAds.Status = entity.Status;
            await _context.SaveChangesAsync();
        }

        //public Stores GetStoreByUserId(int id)
        //{
        //    return _context.Stores
        //        .Where(u => u.UserId == id)
        //        .Include(us => us.User)
        //        .FirstOrDefault(c => c.StoreId == id);

        //}
    }
}

