﻿using marketNaija.Data;
using marketNaija.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using PharmaHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PharmaHub.Repository
{
    public class PatientRepository : _dataRepository<Patients>
    {
        readonly StoresContext _context;

        public PatientRepository(StoresContext context)
        {
            _context = context;
        }

        public async Task Add(Patients entity)
        {
            await _context.Patients.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Patients entity)
        {
             _context.Patients.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<Patients> Get(Guid id)
        {
             return await _context.Patients
                .Include(u => u.User)
                .FirstOrDefaultAsync(i => i.UserId == id || i.DoctorId == id);
        }

        public async Task<IEnumerable<Patients>> GetAll()
        {   
            return await _context.Patients
                .Include(u => u.User)
                .ToListAsync();
        }

        public IEnumerable<Patients> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Patients> GetAllStores()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Patients dbEntity, Patients entity)
        {
            throw new NotImplementedException();
        }
    }
}
