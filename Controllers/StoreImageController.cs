﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Models;
using MarketNaijaApp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
    [Authorize]
    [Route("api/stores/images")]
    [ApiController]
    public class StoreImageController : ControllerBase
    {
      
        private readonly _imageRepository<StoreImage> _dataRepository;

        public StoreImageController(_imageRepository<StoreImage> dataRepository)
        {
            _dataRepository = dataRepository;
         
        }

        [Authorize(Roles = "Admin, Merchant")]
        [HttpPost]
        public IActionResult Post([FromBody]StoreImage filesData)
        {
            if (filesData == null) return BadRequest("Null File");

            _dataRepository.Add(filesData);
            return CreatedAtRoute(
                 "GetImage",
                    new { Id = filesData.ImageId },
                    filesData);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Getimage()
        {

            IEnumerable<StoreImage> files = _dataRepository.GetAll();
            if (files == null)
            {
                return NotFound("The image couldn't be found");
            }
            return Ok(files);

        }


        [AllowAnonymous]
        [HttpGet("{id}" , Name = "GetImage")]
        public IActionResult Get(Guid id)
        {
            StoreImage files = _dataRepository.Get(id);
            if (files == null)
            {
                return NotFound("The image record couldn't be found.");
            }
            return Ok(files);
        }

        [Authorize(Roles = "Admin, Merchant")]
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] StoreImage image)
        {
            if (image == null)
            {
                return BadRequest("image is null ");
            }

            StoreImage ImageToUpdate = _dataRepository.Get(id);
            if (ImageToUpdate == null)
            {
                return NotFound("The image could not be found");
            }

            _dataRepository.Update(ImageToUpdate, image);
            return NoContent();
        }

        [Authorize(Roles = "Admin, Merchant")]
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            StoreImage product = _dataRepository.Get(id);
            if (product == null)
            {
                return NotFound("The image record couldn't be found.");
            }
            _dataRepository.Delete(product);
            return NoContent();
        }
    }
}
