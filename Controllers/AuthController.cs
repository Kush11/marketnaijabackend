﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using marketNaija.Helpers;
using marketNaija.Models;
using marketNaija.Models.DTO;
using marketNaija.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace marketNaija.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/user")]
    public class AuthController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appsettings;

        public AuthController(IUserService userService,
                               IMapper mapper,
                               IOptions<AppSettings>appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appsettings = appSettings.Value;
        }

        // POST api/auth
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]UserDto userDto)
        {
            try
            {
                var users = _userService.Authenticate(username: userDto.Email, password: userDto.Password);

                if (users == null)
                    return BadRequest(new { message = "username or password is incorrect" });

              

                // return basic user info (without password) and token to store client side
                return Ok(new
                {
                    UserId = users.UserId,
                    users.FirstName,
                    users.LastName,
                    users.Address,
                    users.Email,
                    users.PhoneNumber,
                    users.Token,
                    users.ImageUrl
                });

            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]UserDto userDto)
        {
            // map dto to entity
            var user = _mapper.Map<User>(userDto);

            try
            {
                // save 
                _userService.Create(user, userDto.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            var userDtos = _mapper.Map<IList<User>>(users);
            return Ok(userDtos);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            User user = _userService.GetById(id);
            if(user == null)
            {
                return NotFound("The user record couldn't be found.");
            }
           
            return Ok(user);
        }
        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(Guid id, [FromBody]UserDto userDto)
        {
            // map dto to entity and set id
            var user = _mapper.Map<User>(userDto);
            user.UserId = id;

            try
            {
                // save 
                _userService.Update(user, userDto.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        [Authorize(Roles = "Admin, Merchant, User")]
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            _userService.Delete(id);
            return Ok();
        }

    }
}
