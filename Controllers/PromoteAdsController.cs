﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
    [Authorize]
    [Route("api/admin/promote")]
    [ApiController]
    public class PromoteAdsController : ControllerBase
    {
        private readonly _dataRepository<PromoteAds> _dataRepository;
        public PromoteAdsController(_dataRepository<PromoteAds> dataRepository)
        {
            _dataRepository = dataRepository;
        }
        // GET: api/Customers
        [HttpGet, AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            IEnumerable<PromoteAds> promoteAds = await _dataRepository.GetAll();
            return Ok(promoteAds);

        }

        // GET api/customers/5
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetPromotionAds")]
        public async Task<IActionResult> Get(Guid id)
        {
            PromoteAds PromoteAds = await _dataRepository.Get(id);
            if (PromoteAds == null)
            {
                return NotFound("The PromoteAds record couldn't be found.");
            }
            return Ok(PromoteAds);
        }

        // POST: api/Customers
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult Post([FromBody] PromoteAds PromoteAds)
        {
            if (PromoteAds == null)
            {
                return BadRequest("Customer is null");
            }
            _dataRepository.Add(PromoteAds);
            return CreatedAtRoute(
                    "GetPromotionAds",
                    new { Id = PromoteAds.PromotionId },
                    PromoteAds);
        }

        //PUT: api/Customers/5
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Put(Guid id, [FromBody] PromoteAds PromoteAds)
        {
            if (PromoteAds == null)
            {
                return BadRequest("PromoteAds is null ");
            }

            PromoteAds adsToUpdate = await _dataRepository.Get(id);
            if (adsToUpdate == null)
            {
                return NotFound("The customer coould not be found");
            }

            await _dataRepository.Update(adsToUpdate, PromoteAds);
            return NoContent();
        }
        //Delete: api/stores/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            PromoteAds PromoteAds = await _dataRepository.Get(id);
            if (PromoteAds == null)
            {
                return NotFound("The PromoteAds record couldn't be found.");
            }
            await _dataRepository.Delete(PromoteAds);
            return NoContent();
        }
    }
}
