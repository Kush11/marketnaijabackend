﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PharmaHub.Controllers
{
    [Authorize]
    [Route("api/doctor-review")]
    [ApiController]
    public class DoctorReviewController : ControllerBase
    {
        private readonly _dataRepository<DoctorReview> _dataRepository;

        public DoctorReviewController(_dataRepository<DoctorReview> dataRepository)
        {
            _dataRepository = dataRepository;
        }


        // GET: api/Reviews
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetReviews()
        {
            IEnumerable<DoctorReview> reviews = await _dataRepository.GetAll();
            return Ok(reviews);
        }

        // GET: api/Reviews/5
        [Authorize(Roles = "Admin, Doctor, User")]
        [HttpGet("{id}", Name = "GetDoctorReviews")]
        public async Task<IActionResult> GetReview(Guid id)
        {
            DoctorReview review = await _dataRepository.Get(id);
            if (review == null)
            {
                return NotFound("This doctor's review is not available");
            }
            return Ok(review);
        }

        // PUT: api/Reviews/5
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] DoctorReview review)
        {
            if (review == null)
            {
                return BadRequest("review is null");
            }

            DoctorReview reviewToUpdate = await _dataRepository.Get(id);
            if (reviewToUpdate == null)
            {
                return NotFound("This doctor's review could not be found");
            }

            await _dataRepository.Update(reviewToUpdate, review);
            return NoContent();
        }

        // POST: api/Reviews

        //Add Comment
        [AllowAnonymous]
        [HttpPost]
        public IActionResult PostReview([FromBody] DoctorReview review)
        {
            if (review == null)
            {
                return BadRequest("review is null");
            }
            _dataRepository.Add(review);
            return CreatedAtRoute(
                "GetDoctorReviews",
                new { Id = review.ReviewId },
                review);

        }

        // DELETE: api/Reviews/5
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            DoctorReview review = await _dataRepository.Get(id);
            if (review == null)
            {
                return NotFound("This doctor's review record couldn't be found.");
            }
            await _dataRepository.Delete(review);
            return NoContent();
        }
    }
}