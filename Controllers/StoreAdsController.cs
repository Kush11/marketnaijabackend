﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
   
    [Route("api/store/promote")]
    [ApiController]
    public class StoreAdsController : ControllerBase
    {
       
           

        private readonly _dataRepository<StoreAds> _dataRepository;
        private readonly StoresContext _storesContext;

        public StoreAdsController(_dataRepository<StoreAds> dataRepository, StoresContext storesContext)
        {
            _dataRepository = dataRepository;
            _storesContext = storesContext;

        }
        // GET: api/stores


        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<StoreAds> storeAds = await _dataRepository.GetAll();
            return Ok(storeAds);

        }

        // GET api/stores/5

        [HttpGet("{id}", Name = "StoreAds")]
        public async Task<IActionResult> Get(Guid id)
        {
            StoreAds StoreAds = await _dataRepository.Get(id);
            if (StoreAds == null)
            {
                return NotFound("The Ad record couldn't be found.");
            }
            return Ok(StoreAds);
        }

        [Route("StoreAds")]
        [HttpGet]
        public IActionResult GetStoreAds([FromQuery] Guid id)
        {
            IQueryable<StoreAds> StoreAds = _storesContext.StoreAds
                .Where(u => u.AdsId == id);
            if (StoreAds == null)
            {
                return NotFound("The Ad record couldn't be found");
            }
            return Ok(StoreAds);
        }




        [HttpPost]
        public IActionResult Post([FromBody] StoreAds StoreAds)
        {
            if (StoreAds == null)
            {
                return BadRequest("store is null");
            }
            _dataRepository.Add(StoreAds);
            return CreatedAtRoute(
                    "StoreAds",
                    new { Id = StoreAds.AdsId },
                    StoreAds);
        }


        //PUT: api/stores/5

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] StoreAds StoreAds)
        {
            if (StoreAds == null)
            {
                return BadRequest("Ad is null ");
            }

            StoreAds AdToUpdate = await _dataRepository.Get(id);
            if (AdToUpdate == null)
            {
                return NotFound("The store could not be found");
            }

            await _dataRepository.Update(AdToUpdate, StoreAds);
            return NoContent();
        }

        //Delete: api/stores/5

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            StoreAds StoreAds = await _dataRepository.Get(id);
            if (StoreAds == null)
            {
                return NotFound("The ad record couldn't be found.");
            }
            await _dataRepository.Delete(StoreAds);
            return NoContent();
        }

    }
}
