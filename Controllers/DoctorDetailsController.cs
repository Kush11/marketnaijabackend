﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Helpers;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace MarketNaijaApp.Controllers
{
    [Route("api/doctors")]
    [ApiController]
    public class DoctorDetailsController : ControllerBase
    {
        private readonly _dataRepository<DoctorDetails> _dataRepository;
        private readonly IConfiguration configuration;




        public DoctorDetailsController(_dataRepository<DoctorDetails> dataRepository, IConfiguration iConfig)
        {
            _dataRepository = dataRepository;
            configuration = iConfig;

        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<DoctorDetails> doctors = await _dataRepository.GetAll();
            return Ok(doctors);

        }


        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetDoctorDetails")]
        public async Task<IActionResult> Get(Guid id)
        {
            DoctorDetails doctor = await _dataRepository.Get(id);
            if (doctor == null)
            {
                return NotFound("The doctor record couldn't be found.");
            }
            string accountSid = configuration.GetValue<string>("TwilioConfig:AccountSid");
            string authToken = configuration.GetValue<string>("TwilioConfig:AuthToken");

            TwilioClient.Init(accountSid, authToken);

            var token = TokenResource.Create();
            doctor.User.ConnectionId = token.Username;
            doctor.User.ConnectionPass = token.Password;
            return Ok(doctor);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Add([FromBody] DoctorDetails doctor)
        {
            if(doctor == null)
            {
                return BadRequest("doctor is null");
            }
            _dataRepository.Add(doctor);
            return CreatedAtRoute("GetDoctorDetails",
                new { Id = doctor.DoctorId },
                doctor);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] DoctorDetails doctor)
        {
            if (doctor == null)
            {
                return BadRequest("doctor is null ");
            }

            DoctorDetails doctorToUpdate = await _dataRepository.Get(id);
            if (doctorToUpdate == null)
            {
                return NotFound("The doctor could not be found");
            }

            await _dataRepository.Update(doctorToUpdate, doctor);
            return NoContent();
        }

        [Authorize(Roles = "Admin, Doctor")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            DoctorDetails doctor = await _dataRepository.Get(id);
            if (doctor == null)
            {
                return NotFound("The doctor record couldn't be found.");
            }
            await _dataRepository.Delete(doctor);
            return NoContent();
        }


        
    }
}