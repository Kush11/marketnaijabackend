﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
    [Route("api/prescriptions")]
    [ApiController]
    public class PrescriptionController : ControllerBase
    {
        private readonly _dataRepository<Prescription> _dataRepository;


        public PrescriptionController(_dataRepository<Prescription> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Prescription> prescriptions = await _dataRepository.GetAll();
            return Ok(prescriptions);

        }


        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetPrescription")]
        public async Task<IActionResult> Get(Guid id)
        {
            Prescription prescription = await _dataRepository.Get(id);
            if (prescription == null)
            {
                return NotFound("The prescription record couldn't be found.");
            }
            return Ok(prescription);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Add([FromBody] Prescription prescription)
        {
            if (prescription == null)
            {
                return BadRequest("prescription is null");
            }
            _dataRepository.Add(prescription);
            return CreatedAtRoute("GetPrescription",
                new { Id = prescription.PrescriptionId },
                prescription);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Prescription prescription)
        {
            if (prescription == null)
            {
                return BadRequest("prescription is null ");
            }

            Prescription prescriptionToUpdate = await _dataRepository.Get(id);
            if (prescriptionToUpdate == null)
            {
                return NotFound("The prescription could not be found");
            }

            await _dataRepository.Update(prescriptionToUpdate, prescription);
            return NoContent();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Prescription prescription = await _dataRepository.Get(id);
            if (prescription == null)
            {
                return NotFound("The prescription record couldn't be found.");
            }
            await _dataRepository.Delete(prescription);
            return NoContent();
        }

    }
}
