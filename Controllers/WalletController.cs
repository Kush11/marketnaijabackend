﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
    [Authorize]
    [Route("api/store/wallet")]
    [ApiController]
    public class WalletController : ControllerBase
    {
            private readonly _dataRepository<Wallet> _dataRepository;

            public WalletController(_dataRepository<Wallet> dataRepository)
            {
                _dataRepository = dataRepository;

            }
            // GET: api/stores


            [HttpGet]
            public async Task<IActionResult> Get()
            {
                IEnumerable<Wallet> wallet = await _dataRepository.GetAll();
                return Ok(wallet);

            }

            // GET api/stores/5
            [Authorize(Roles = "Admin, Merchant")]
            [HttpGet("{id}", Name = "GetWallet")]
            public async Task<IActionResult> Get(Guid id)
            {
                Wallet wallet = await _dataRepository.Get(id);
                if (wallet == null)
                {
                    return NotFound("The wallet record couldn't be found.");
                }
                return Ok(wallet);
            }

            [Authorize(Roles = "Merchant")]
            [HttpPost]
            public IActionResult Post([FromBody] Wallet wallet)
            {
                if (wallet == null)
                {
                    return BadRequest("store is null");
                }
                _dataRepository.Add(wallet);
                return CreatedAtRoute(
                        "GetWallet",
                        new { Id = wallet.walletId },
                        wallet);
            }

         

            //PUT: api/stores/5
            [Authorize(Roles = "Admin, Merchant")]
            [HttpPut("{id}")]
            public async Task<IActionResult> Put(Guid id, [FromBody] Wallet wallet)
            {
                if (wallet == null)
                {
                    return BadRequest("store is null ");
                }

                Wallet walletToUpdate = await _dataRepository.Get(id);
                if (walletToUpdate == null)
                {
                    return NotFound("The store could not be found");
                }

                _dataRepository.Update(walletToUpdate, wallet);
                return NoContent();
            }

            //Delete: api/stores/5
            [Authorize(Roles = "Admin, Merchant")]
            [HttpDelete("{id}")]
            public async Task<IActionResult> Delete(Guid id)
            {
                Wallet wallet = await _dataRepository.Get(id);
                if (wallet == null)
                {
                    return NotFound("The stores record couldn't be found.");
                }
                _dataRepository.Delete(wallet);
                return NoContent();
            }

        }
    }