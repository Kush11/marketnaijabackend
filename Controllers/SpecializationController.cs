﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
    [Route("api/specializations")]
    [ApiController]
    public class SpecializationController : ControllerBase
    {
        private readonly _dataRepository<DoctorSpecialization> _specializationRepository;

        private readonly StoresContext _storesContext;

        public SpecializationController(StoresContext storesContext, _dataRepository<DoctorSpecialization> specializationRepository)
        {
            _storesContext = storesContext;
            _specializationRepository = specializationRepository;

        }

      
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetSpecializations()
        {
            IEnumerable<DoctorSpecialization> specializations = await _specializationRepository.GetAll();
            return Ok(specializations);

        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult AddSpecialization([FromBody] DoctorSpecialization specialization)
        {
            if (specialization == null)
            {
                return BadRequest("specialization is null");
            }
            _storesContext.DoctorSpecialization.Add(specialization);
            return CreatedAtRoute("GetDoctorSpecialization",
                new { Id = specialization.SpecializationId },
                specialization);
        }
    }
}