﻿
using System;
using System.Collections.Generic;
using System.Linq;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
    [Authorize]
    [Route("/api/products/productImage")]
    [ApiController]
    public class ProductImageController : ControllerBase
    {


        private readonly _imageRepository<file> _dataRepository;

        public ProductImageController(_imageRepository<file> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [Authorize(Roles = "Admin, Merchant")]
        [HttpPost]
        public IActionResult Post([FromBody]file filesData)
        {
            if (filesData == null) return BadRequest("Null File");

            _dataRepository.Add(filesData);
            return CreatedAtRoute(
                 "GetProductImage",
                    new { Id = filesData.FileUploadId },
                    filesData);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Getimage()
        {

            IEnumerable<file> files = _dataRepository.GetAll();
            if (files == null)
            {
                return NotFound("The image couldn't be found");
            }
            return Ok(files);

        }

        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetProductImage")]
        public IActionResult Get(Guid id)
        {
            file files = _dataRepository.Get(id);
            if (files == null)
            {
                return NotFound("The product record couldn't be found.");
            }
            return Ok(files);
        }
    }
}