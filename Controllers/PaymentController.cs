﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using marketNaija.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PharmaHub.Models;

namespace PharmaHub.Controllers
{
    [Authorize]
    [Route("api/payment")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly _dataRepository<PaymentData> _dataRepository;

        public PaymentController(_dataRepository<PaymentData> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [Authorize(Roles = "Admin, Merchant, Doctor, User")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<PaymentData> payment =  await _dataRepository.GetAll();
            return Ok(payment);

        }

        // GET api/payment/5
        [Authorize(Roles = "Admin, Merchant, Doctor, User")]
        [HttpGet("{id}", Name = "GetPaymentToken")]
        public async Task<IActionResult> Get(Guid id)
        {
            PaymentData payment =  await _dataRepository.Get(id);
            if (payment == null)
            {
                return NotFound("The payment token couldn't be found.");
            }
            return Ok(payment);
        }




        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody] PaymentData payment)
        {
            if (payment == null)
            {
                return BadRequest("payment token is null");
            }
             _dataRepository.Add(payment);
            return CreatedAtRoute(
                    "GetPaymentToken",
                    new { Id = payment.PaymentId },
                    payment);
        }

        [AllowAnonymous]
        [HttpPost("verify")]
        public string VerifyPayment([FromBody] VerifyPayment payment)
        {
            var data = new { txref = payment.TxRex, SECKEY = payment.SecKey };
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var responseMessage = client.PostAsJsonAsync("https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify", data).Result;
            //please make sure to change this to production url when you go live
            var responseStr = responseMessage.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<ResponseData>(responseStr);
            if (response.data.status == "successful" && response.data.amount == payment.Amount && response.data.chargecode == "00")
            {
                return (response.data.status);
            }
            else
            {
                return ("The transaction was not successful");

            }
        }


        //PUT: api/Payment/5
        [Authorize(Roles = "Admin, Merchant, Doctor, User")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] PaymentData payment)
        {

            if (payment == null)
            {
                return BadRequest("payment token is null ");
            }

            PaymentData tokenToUpdate =  await _dataRepository.Get(id);
            if (tokenToUpdate == null)
            {
                return NotFound("The payment token could not be found");
            }

            await _dataRepository.Update(tokenToUpdate, payment);
            return Ok();
        }

        //Delete: api/payment/5
        [Authorize(Roles = "Admin, Merchant, Doctor, User")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            PaymentData payment = await _dataRepository.Get(id);
            if (payment == null)
            {
                return NotFound("The payment token could not be found.");
            }
            await _dataRepository.Delete(payment);
            return NoContent();
        }
    }
}