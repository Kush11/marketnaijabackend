﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Models.DTO;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using MarketNaijaApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace marketNaija.Controllers
{

    [Route("api/homestores")]
    [ApiController]
    public class HomeStoresController : Controller
    {
        private readonly _dataRepository<Stores> _dataRepository;
        private readonly IMapper _mapper;


        public HomeStoresController(_dataRepository<Stores> dataRepository, IMapper mapper)
        {
            _dataRepository = dataRepository;
            _mapper = mapper;

        }
        // GET: api/stores

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            var stores = _dataRepository.GetAllStores();
            var storesDtos = _mapper.Map<IList<Stores>>(stores);
            return Ok(storesDtos);
        }

       
    }
}
