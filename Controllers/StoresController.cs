﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using MarketNaijaApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace marketNaija.Controllers
{

    [Route("api/stores")]
    [ApiController]
    public class StoresController : Controller
    {
        private readonly _dataRepository<Stores> _dataRepository;
        private readonly StoresContext _storesContext;
        private readonly IEmailService _emailService;

        public StoresController(_dataRepository<Stores> dataRepository, IEmailService emailService, StoresContext storesContext)
        {
            _dataRepository = dataRepository;
            _emailService = emailService;
            _storesContext = storesContext;

        }
        // GET: api/stores

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Stores> stores = await _dataRepository.GetAll();
            return Ok(stores);

        }

        // GET api/stores/5
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetStores")]
        public async Task<IActionResult> Get(Guid id)
        {
            Stores stores = await _dataRepository.Get(id);
            if (stores == null)
            {
                return NotFound("The store record couldn't be found.");
            }
            return Ok(stores);
        }
        [AllowAnonymous]
        [Route("users")]
        [HttpGet]
        public  IActionResult GetStore([FromQuery] Guid id)
        {
            IQueryable<Stores> stores =  _storesContext.Stores
                .Where(u => u.UserId == id);
            if(stores == null)
            {
                return NotFound("The store record couldn't be found");
            }
            return Ok(stores);
        }
       


       [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody] Stores stores)
        {
            if (stores == null)
            {
                return BadRequest("store is null");
            }
            _dataRepository.Add(stores);
            return CreatedAtRoute(
                    "GetStores",
                    new { Id = stores.StoreId },
                    stores);
        }

        //Post: api/stores/ send-email
        [Route("send-email")]
        public async Task<IActionResult> SendEmailAsync([FromQuery] string username, string subject, string message)
        {
            await _emailService.SendEmailAsync(username, subject, message);
            return Ok();
        }
        
        //PUT: api/stores/5
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Stores stores)
        {
            if (stores == null)
            {
                return BadRequest("store is null ");
            }

            Stores storeToUpdate = await _dataRepository.Get(id);
            if (storeToUpdate == null)
            {
                return NotFound("The store could not be found");
            }

            await _dataRepository.Update(storeToUpdate, stores);
            return NoContent();
        }

        //Delete: api/stores/5
        [Authorize(Roles = "Admin, Merchant")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Stores stores = await _dataRepository.Get(id);
            if (stores == null)
            {
                return NotFound("The stores record couldn't be found.");
            }
            await _dataRepository.Delete(stores);
            return NoContent();
        }

    }
}
