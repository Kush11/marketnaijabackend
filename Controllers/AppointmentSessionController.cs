﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{
    [Route("api/sessions")]
    [ApiController]
    public class AppointmentSessionController : ControllerBase
    {
        private readonly _dataRepository<AppointmentSession> _dataRepository;


        public AppointmentSessionController(_dataRepository<AppointmentSession> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<AppointmentSession> sessions = await _dataRepository.GetAll();
            return Ok(sessions);

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("doctor")]
        public async Task<IActionResult> GetByDoctorId([FromQuery]Guid id)
        {
            IEnumerable<AppointmentSession> sessions = await _dataRepository.GetAll();
            var allDoctorSessions = sessions.Where(d => d.DoctorId == id);
            return Ok(allDoctorSessions);

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("patient")]
        public async Task<IActionResult> GetByPatientId([FromQuery]Guid id)
        {
            IEnumerable<AppointmentSession> sessions = await _dataRepository.GetAll();
            var allPatienSessions = sessions.Where(d => d.Patient.UserId == id);
            return Ok(allPatienSessions);

        }


        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetAppointmentSession")]
        public async Task<IActionResult> Get(Guid id)
        {
            AppointmentSession session = await _dataRepository.Get(id);
            if (session == null)
            {
                return NotFound("The session record couldn't be found.");
            }
            return Ok(session);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Add([FromBody] AppointmentSession session)
        {
            if (session == null)
            {
                return BadRequest("session is null");
            }
            //_storesContext.AppointmentSessions.Add(session);
            _dataRepository.Add(session);
            return CreatedAtRoute("GetAppointmentSession",
                new { Id = session.SessionId },
                session);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] AppointmentSession session)
        {
            if (session == null)
            {
                return BadRequest("session is null ");
            }

            AppointmentSession sessionToUpdate = await _dataRepository.Get(id);
            if (sessionToUpdate == null)
            {
                return NotFound("The session could not be found");
            }

            await _dataRepository.Update(sessionToUpdate, session);
            return NoContent();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            AppointmentSession session = await _dataRepository.Get(id);
            if (session == null)
            {
                return NotFound("The session record couldn't be found.");
            }
            await _dataRepository.Delete(session);
            return NoContent();
        }
       
    }
}