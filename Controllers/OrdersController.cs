﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace marketNaija.Controllers
{
    [Authorize]
    [Route("api/orders")]
    public class OrdersController : Controller
    {
        private readonly _dataRepository<Orders> _dataRepository;
        private readonly StoresContext _storesContext;

        public OrdersController(_dataRepository<Orders> dataRepository, StoresContext storesContext)
        {
            _dataRepository = dataRepository;
            _storesContext = storesContext;
        }
        // GET: api/orders
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Orders> orders = await _dataRepository.GetAll();
            return Ok(orders);

        }

        // GET api/orders/5
        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetOrders")]
        public async Task<IActionResult> Get(Guid id)
        {
            Orders order = await _dataRepository.Get(id);
            if (order == null)
            {
                return NotFound("The Order record couldn't be found.");
            }
            return Ok(order);
        }

        // POST: api/orders
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post([FromBody] Orders orders)
        {
            if (orders == null)
            {
                return BadRequest("Order is null");
            }
            _dataRepository.Add(orders);
            return CreatedAtRoute(
                    "GetOrders",
                    new { Id = orders.OrderId },
                    orders);
        }

        [Route("store")]
        [HttpGet]
        public IActionResult GetStore([FromQuery] Guid id)
        {
            IQueryable<Orders> orders = _storesContext.Orders
                .Where(u => u.storeId == id);
            if (orders == null)
            {
                return NotFound("The order record couldn't be found");
            }
            return Ok(orders);
        }

        //PUT: api/orders/5
        [Authorize(Roles = "Merchant")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Orders order)
        {
            if (order == null)
            {
                return BadRequest("Order is null ");
            }

            Orders orderToUpdate = await _dataRepository.Get(id);
            if (orderToUpdate == null)
            {
                return NotFound("The order could not be found");
            }

            await _dataRepository.Update(orderToUpdate, order);
            return NoContent();
        }

        //Delete: api/orders/5
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Orders order = await _dataRepository.Get(id);
            if (order == null)
            {
                return NotFound("The order record couldn't be found.");
            }
            await _dataRepository.Delete(order);
            return NoContent();
        }
    }
}
