﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MarketNaijaApp.Models;
using marketNaija.Data;
using Microsoft.AspNetCore.Authorization;
using marketNaija.Repository.Interface;

namespace MarketNaijaApp.Controllers
{
    [Authorize]
    [Route("api/review")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly _dataRepository<Review> _dataRepository;

        public ReviewsController(_dataRepository<Review> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        // GET: api/Reviews
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetReviews()
        {
            IEnumerable<Review> reviews = await _dataRepository.GetAll();
            return Ok(reviews);
        }

        // GET: api/Reviews/5
        [Authorize(Roles = "Admin, Merchant, Customer")]
        [HttpGet("{id}", Name = "GetReviews")]
        public async Task<IActionResult> GetReview(Guid id)
        {
            Review review = await _dataRepository.Get(id);
            if(review == null)
            {
                return NotFound("The store review is not available");
            }
            return Ok(review);
        }

        // PUT: api/Reviews/5
        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Review review)
        {
            if (review == null)
            {
                return BadRequest("review is null");
            }

            Review reviewToUpdate = await _dataRepository.Get(id);
            if (reviewToUpdate == null)
            {
                return NotFound("The store review could not be found");
            }

            await _dataRepository.Update(reviewToUpdate, review);
            return NoContent();
        }

        // POST: api/Reviews

        //Add Comment
        [AllowAnonymous]
        [HttpPost]
        public IActionResult PostReview([FromBody] Review review)
        {
            if (review == null)
            {
                return BadRequest("review is null");
            }
            _dataRepository.Add(review);
            return CreatedAtRoute(
                "GetReviews",
                new { review.Id },
                review);

        }

        // DELETE: api/Reviews/5
        [Authorize(Roles = "Admin, Merchant")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Review review = await _dataRepository.Get(id);
            if (review == null)
            {
                return NotFound("The stores review record couldn't be found.");
            }
            await _dataRepository.Delete(review);
            return NoContent();
        }

       
    }
}