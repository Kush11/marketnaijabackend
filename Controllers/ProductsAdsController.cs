﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using MarketNaijaApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MarketNaijaApp.Controllers
{   
    [Route("api/product/promote")]
    [ApiController]
    public class ProductsAdsController : ControllerBase
    {
            private readonly _dataRepository<ProductAds> _dataRepository;
            private readonly StoresContext _storesContext;

            public ProductsAdsController(_dataRepository<ProductAds> dataRepository, StoresContext storesContext)
            {
                _dataRepository = dataRepository;
                _storesContext = storesContext;

            }
            // GET: api/stores

           
            [HttpGet]
            public async Task<IActionResult> Get()
            {
                IEnumerable<ProductAds> productAds = await _dataRepository.GetAll();
                return Ok(productAds);

            }

            // GET api/stores/5
            
            [HttpGet("{id}", Name = "GetProductAds")]
            public async Task<IActionResult> Get(Guid id)
            {
            ProductAds ProductAds = await _dataRepository.Get(id);
                if (ProductAds == null)
                {
                    return NotFound("The Ad record couldn't be found.");
                }
                return Ok(ProductAds);
            }
            
            [Route("productsAds")]
            [HttpGet]
            public IActionResult GetProductAds([FromQuery] Guid id)
            {
                IQueryable<ProductAds> ProductAds = _storesContext.ProductAds
                    .Where(u => u.ProductId == id);
                if (ProductAds == null)
                {
                    return NotFound("The Ad record couldn't be found");
                }
                return Ok(ProductAds);
            }



           
            [HttpPost]
            public IActionResult Post([FromBody] ProductAds ProductAds)
            {
                if (ProductAds == null)
                {
                    return BadRequest("store is null");
                }
                _dataRepository.Add(ProductAds);
                return CreatedAtRoute(
                        "GetProductAds",
                        new { Id = ProductAds.ProductId },
                        ProductAds);
            }

            
            //PUT: api/stores/5
           
            [HttpPut("{id}")]
            public async Task<IActionResult> Put(Guid id, [FromBody] ProductAds ProductAds)
            {
                if (ProductAds == null)
                {
                    return BadRequest("Ad is null ");
                }

            ProductAds AdToUpdate = await _dataRepository.Get(id);
                if (AdToUpdate == null)
                {
                    return NotFound("The store could not be found");
                }

                await _dataRepository.Update(AdToUpdate, ProductAds);
                return NoContent();
            }

            //Delete: api/stores/5
            
            [HttpDelete("{id}")]
            public async Task<IActionResult> Delete(Guid id)
            {
            ProductAds ProductAds = await _dataRepository.Get(id);
                if (ProductAds == null)
                {
                    return NotFound("The ad record couldn't be found.");
                }
                await _dataRepository.Delete(ProductAds);
                return NoContent();
            }

        }
    }