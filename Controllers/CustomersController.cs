﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace marketNaija.Controllers
{
    [Authorize]
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly _dataRepository<Customers> _dataRepository;
        private readonly StoresContext _storesContext;

        public CustomersController(_dataRepository<Customers> dataRepository, StoresContext storesContext)
        {
            _dataRepository = dataRepository;
            _storesContext = storesContext;

        }
        // GET: api/Customers
        [HttpGet, Authorize(Roles = "Admin")]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Customers> customers = await _dataRepository.GetAll();
            return Ok(customers);

        }

        // GET api/customers/5
        [Authorize(Roles = "Admin, Merchant, Customer")]
        [HttpGet("{id}", Name = "GetCustomers")]
        public async Task<IActionResult> Get(Guid id)
        {
            Customers customer = await _dataRepository.Get(id);
            if (customer == null)
            {
                return NotFound("The Customer record couldn't be found.");
            }
            return Ok(customer);
        }

        // POST: api/Customers
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Customers customer)
        {
            if (customer == null)
            {
                return BadRequest("Customer is null");
            }
            await _dataRepository.Add(customer);
            return Ok();
        }

        //PUT: api/Customers/5
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin, User")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Customers customer)
        {
            if(customer == null)
            {
                return BadRequest("Customer is null ");
            }

            Customers customerToUpdate = await _dataRepository.Get(id);
            if(customerToUpdate == null)
            {
                return NotFound("The customer coould not be found");
            }

            await _dataRepository.Update(customerToUpdate, customer);
            return NoContent();
        }
        //Delete: api/stores/5
        [Authorize(Roles = "Admin, Merchant, Customer")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Customers customer =await _dataRepository.Get(id);
            if(customer == null)
            {
                return NotFound("The Employee record couldn't be found.");
            }
            await _dataRepository.Delete(customer);
            return NoContent();
        }

        [AllowAnonymous]
        [Route("store")]
        [HttpGet]
        public IActionResult GetCustomerByStoreId([FromQuery] Guid id)
        {
            IQueryable<Customers> customer = _storesContext.Customers
                .Where(u => u.StoreId == id)
                .Include(us => us.User);
            if (customer == null)
            {
                return NotFound("The customer record couldn't be found");
            }
            return Ok(customer);
        }
    }
}
