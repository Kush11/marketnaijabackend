﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using marketNaija.Data;
using marketNaija.Models;
using marketNaija.Models.DTO;
using marketNaija.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace marketNaija.Controllers
{
    [Authorize]
    [Route("api/products")]
    public class ProductsController : Controller
    {
        private readonly _dataRepository<Products> _dataRepository;
        private readonly IMapper _mapper;
        private readonly StoresContext _storesContext;

        public ProductsController(_dataRepository<Products> dataRepository, IMapper mapper, StoresContext storesContext)
        {
            _dataRepository = dataRepository;
            _storesContext = storesContext;
            _mapper = mapper;

        }
        // GET: api/products
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Products> products = await _dataRepository.GetAll();
            return Ok(products);

        }

        // GET api/products/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            Products products = await _dataRepository.Get(id);
            if (products == null)
            {
                return NotFound("The product record couldn't be found.");
            }
            return Ok(products);
        }

        // GET api/homeproducts

        [AllowAnonymous]
        [Route("homeproduct")]
        [HttpGet]
        public IActionResult GeProductst()
        {
            var products = _dataRepository.GetAllProducts();
            var productsDtos = _mapper.Map<IList<Products>>(products);
            return Ok(productsDtos);

        }

        [AllowAnonymous]
        [Route("image")]
        [HttpGet]
        public IActionResult GetProduct([FromQuery] Guid id)
        {
            IQueryable<file> files = _storesContext.Image
                .Where(u => u.ProductId == id);
            if (files == null)
            {
                return NotFound("The product image record couldn't be found");
            }
            return Ok(files);
        }

        // POST: api/products
        [Authorize(Roles ="Admin, Merchant")]
        [HttpPost]
        public IActionResult Post([FromBody]Products product)
        {
            if (product == null)
            {
                return BadRequest("product is null");
            }
            _dataRepository.Add(product);
            return Ok();
        }

        //PUT: api/products/5
        [Authorize(Roles ="Admin, Merchant")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Products product)
        {
            if (product == null)
            {
                return BadRequest("product is null ");
            }

            Products productToUpdate = await _dataRepository.Get(id);
            if (productToUpdate == null)
            {
                return NotFound("The product could not be found");
            }

            await _dataRepository.Update(productToUpdate, product);
            return NoContent();
        }
        //Delete: api/product/5
        [Authorize(Roles ="Admin, Merchant")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Products product = await _dataRepository.Get(id);
            if (product == null)
            {
                return NotFound("The product record couldn't be found.");
            }
            await _dataRepository.Delete(product);
            return NoContent();
        }
    }
}
