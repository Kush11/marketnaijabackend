﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using marketNaija.Data;
using marketNaija.Repository.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PharmaHub.Models;

namespace PharmaHub.Controllers
{
    [Route("api/patients")]
    [ApiController]
    public class PatientsController : ControllerBase
    {
        private readonly _dataRepository<Patients> _dataRepository;


        public PatientsController(_dataRepository<Patients> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<Patients> patients = await _dataRepository.GetAll();
            return Ok(patients);

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("doctor")]
        public async Task<IActionResult> GetByDoctorId([FromQuery]Guid id)
        {
            IEnumerable<Patients> patients = await _dataRepository.GetAll();
            var allDoctorPatients = patients.Where(d => d.DoctorId == id);
            return Ok(allDoctorPatients);

        }

        //[AllowAnonymous]
        //[HttpGet]
        //[Route("patient")]
        //public IActionResult GetByPatientId([FromQuery]Guid id)
        //{
        //    IEnumerable<AppointmentSession> sessions = _dataRepository.GetAll();
        //    var allDoctorSessions = sessions.Where(d => d.PatientId == id);
        //    return Ok(allDoctorSessions);

        //}


        [AllowAnonymous]
        [HttpGet("{id}", Name = "GetPatient")]
        public async Task<IActionResult> Get(Guid id)
        {
            Patients patients = await _dataRepository.Get(id);
            return Ok(patients);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Add([FromBody] Patients patient)
        {
            if (patient == null)
            {
                return BadRequest("patient is null");
            }
            _dataRepository.Add(patient);
            return CreatedAtRoute("GetPatient",
                new { Id = patient.PatientId },
                patient);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] Patients patient)
        {
            if (patient == null)
            {
                return BadRequest("patient is null ");
            }

            Patients patientToUpdate = await _dataRepository.Get(id);
            if (patientToUpdate == null)
            {
                return NotFound("The patient could not be found");
            }

            await _dataRepository.Update(patientToUpdate, patient);
            return NoContent();
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Patients patient = await _dataRepository.Get(id);
            if (patient == null)
            {
                return NotFound("The patient record couldn't be found.");
            }
            await _dataRepository.Delete(patient);
            return NoContent();
        }
    }
}