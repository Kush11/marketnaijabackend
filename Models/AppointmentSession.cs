﻿using marketNaija.Models;
using PharmaHub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Models
{
    public class AppointmentSession
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SessionId { get; set; }
        public Guid PatientId { get; set; }
        public Guid DoctorId { get; set; }
        public string SessionStartDateTime { get; set; }
        public string SessionEndDateTime { get; set; }
        public string Status { get; set; }
        public string SessionCode { get; set; }
        public string PatientAilment { get; set; }
        public string Diagnosis { get; set; }
        public bool IsPaymentDone { get; set; }

        [ForeignKey("PatientId")]
        public virtual Patients Patient { get; set; }
        public ICollection<Prescription> Prescriptions { get; set; }

      
    }
}
