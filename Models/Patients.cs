﻿using marketNaija.Models;
using MarketNaijaApp.Models;
using MarketNaijaApp.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PharmaHub.Models
{
    public class Patients
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public Guid PatientId { get; set; }
        public Guid UserId { get; set; }
        public Guid DoctorId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
