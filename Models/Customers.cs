﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models
{
    public class Customers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CustomerId                                     { get; set; }
        public string CreatedOn                                    { get; set; }

       
        public string UserName                                    { get; set; }
        public Guid? StoreId                                      { get; set; }
        
        public Guid UserId                                        { get; set; }
      
        [ForeignKey("UserId")]
        public virtual User User                                  { get; set; }
        [ForeignKey("StoreId")]
        public virtual Stores Store { get; set; }


    }
}
