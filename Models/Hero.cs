﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Models
{
    public class Hero
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid FileUploadId { get; set; }
        [Required]
        public byte[] Banner_One { get; set; }
        public byte[] Banner_Two { get; set; }
        public byte[] Banner_Three { get; set; }

    }
}
