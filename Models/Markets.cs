﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models
{
    public class Markets
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MarketId               { get; set; }

        [Required(ErrorMessage = "Market Name Is Required")]
        [StringLength(60, ErrorMessage ="Market Name Can not be longer than 60 characters")]
        public string MarketName          { get; set; }
        public string Description         { get; set; }

        public string Location            { get; set; }
        public ICollection<Stores>Stores  { get; set; }

        public virtual MarketImage MarketImage { get; set ; }


    }

    public class MarketImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid FileUploadId { get; set; }
        [Required]
        public byte[] MarketBanner { get; set; }

        public Guid MarketId { get; set; }
        [ForeignKey("MarketId")]
        public virtual Markets Market { get; set; }

    }
}
