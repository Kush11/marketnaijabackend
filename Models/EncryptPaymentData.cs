﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PharmaHub.Models
{
    public class EncryptPaymentData
    {
        public string PBFPubKey { get; set; }
        public string cardno { get; set; }
        public string cvv { get; set; }
        public string expirymonth { get; set; }
        public string expiryyear { get; set; }
        public string currency { get; set; }
        public string country { get; set; }
        public string amount { get; set; }
        public string redirectUrl { get; set; }
        public string txRef { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string firstName { get; set; }
        public string billingzip { get; set; }
        public string billingcity { get; set; }
        public string billingaddress { get; set; }
        public string billingstate { get; set; }
        public string billingcountry { get; set; }
        public string suggested_auth { get; set; }
        public string pin { get; set; }
        public string OTP { get; set; }
    }

    public class EncryptedData
    {
        public string PaymentData { get; set; }
    }
}
