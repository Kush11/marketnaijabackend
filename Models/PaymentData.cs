﻿using marketNaija.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PharmaHub.Models
{
    public class PaymentData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PaymentId { get; set; }
        public Guid UserId { get; set; }
        public string PaymentToken { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
