﻿using LiteDB;
using marketNaija.Models;
using MarketNaijaApp.Repository;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Models
{
    public class NotifyHub : Hub<ITypedHubClient>
    {

      

        public override async Task OnConnectedAsync()
        {

            Console.WriteLine("Who is connected: " + Context.ConnectionId);
            await base.OnConnectedAsync();

        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);
        }
        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

        }

        public async Task CallGroupMember(string groupName, string message)
        {
            await Clients.GroupExcept(groupName, Context.ConnectionId).CallGroupMember(message);
        }

        public async Task EndGroupCall(string groupName, string message)
        {
            await Clients.OthersInGroup(groupName).EndGroupCall(message);
        }
        public async Task SendSignal(string groupName, string signal)
        {
            await Clients.GroupExcept(groupName, Context.ConnectionId).SendSignal(signal);
        }

        public async Task ReceiveGroupMessage(string groupName, string message)
        {
            await Clients.GroupExcept(groupName, Context.ConnectionId).ReceiveGroupMessage(message);
        }
        public async Task ReceiveSignal(string groupName, string signal)
        {
            await Clients.GroupExcept(groupName, Context.ConnectionId).ReceiveSignal(signal);
        }

        public async Task LeaveRoom(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
    }
}
