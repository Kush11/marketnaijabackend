﻿using marketNaija.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Models
{
    public class Review
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id                { get; set; }
        public Guid StoreId           { get; set; }
        public string Comment        { get; set; }
        public string CommentBy { get; set; }
        public string CommentedOn    { get; set; }

        [ForeignKey("StoreId")]
        public virtual Stores Store  { get; set; }
        public int UserRate          { get; set; }
    }

    public class DoctorReview
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ReviewId { get; set; }
        public Guid DoctorId { get; set; }
        public string Comment { get; set; }
        public string CommentBy { get; set; }
        public string CommentedOn { get; set; }
        public int UserRate { get; set; }

        [ForeignKey("DoctorId")]
        public virtual DoctorDetails DoctorDetails { get; set; }



    }
}
