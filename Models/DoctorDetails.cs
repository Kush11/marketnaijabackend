﻿using marketNaija.Models;
using PharmaHub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Models
{
    public class DoctorDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DoctorId  { get; set; }
        public Guid UserId { get; set; }
        public Guid SpecializationId { get; set; }

        public string LicenseUrl { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public string WorkingDays { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Rating { get; set; }
        [ForeignKey("SpecializationId")]
        public virtual DoctorSpecialization Specialization { get; set; }
        public string Status { get; set; }
        public bool IsAvailable { get; set; }
        public string AccountNumber { get; set; }
        public string DoctorBank { get; set; }
        public string PaymentAccountId { get; set; }
    }
}
