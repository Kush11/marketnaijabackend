﻿using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace marketNaija.Models
{
    public class Products
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ProductId                        { get; set; }
        public string ProductName                    { get; set; }
        public int Price                             { get; set; }
        public int Discount                          { get; set; }
        //public byte[] File                           { get; set; }
        public string Description                    { get; set; }
        public Guid StoreId                          { get; set; }
        public string Brand                          { get; set; }
        public string ImageUrl                       { get; set; }
        public string Category                       { get; set; }
        public string CreatedOn                      { get; set; }
        public string Specification                  { get; set; }
        public string Location                       { get; set; }
        [ForeignKey("StoreId")]
        public virtual Stores Store                  { get; set; }
        public virtual file file                     { get; set; }
        public virtual ProductAds ProductAds         { get; set; }
     
    }

    public class file
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid FileUploadId { get; set; }
        [Required]
        public byte[] File { get; set; }
        public Guid ProductId { get; set; }


        [ForeignKey("ProductId")]
        public virtual Products Products { get; set; }

    }

    public class ProductAds
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AdsId { get; set; }
        public Guid ProductId { get; set; }

        public Guid PromotionId  {get; set;}
        public string Status { get; set; }
        public bool Discount { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        [ForeignKey("ProductId")]
        public virtual Products Products { get; set; }
        [ForeignKey("PromotionId")]
        public virtual PromoteAds PromoteAds { get; set; }

    }
}
