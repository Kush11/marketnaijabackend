﻿using MarketNaijaApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models
{
    public class Stores
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid StoreId                                         { get; set; }

       
        [StringLength(60, ErrorMessage = "Name Can not be longer than 60 characters")]
        public string StoreName                                    { get; set; }

       
        public string Email                                        { get; set; }

        public string CreatedOn                                    { get; set; }
        public string ImageUrl                                     { get; set; }
       
        public string Description                                  { get; set; }
      
        public string PhoneNumber                                  { get; set; }

        public double Rating                                       { get; set; }
        public bool ConfirmStatus                                  { get; set; }
        public string StoreType                                    { get; set; }
        public string Address                                      { get; set; }
        public string StoreUrl                                     { get; set; }
        public Guid UserId                                         { get; set; }
        [ForeignKey("UserId")]
        public virtual User User                                   { get; set; }
        
        public ICollection<Orders> Orders                          { get; set; }
        public virtual ICollection<Products> Products              { get; set; }
        //public virtual ICollection<Customers> Customers { get; set; }
        public virtual ICollection<Review> Review                  { get; set; }
        public virtual StoreImage StoreImage                       { get; set; }
        public virtual StoreAds StoreAds                           { get; set; }
        public virtual Wallet Wallet { get; set; }


    }

    public class StoreImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ImageId { get; set; }
        [Required]
        public byte[] File { get; set; }

        public Guid StoreId { get; set; }
        [ForeignKey("StoreId")]
        public virtual Stores Store { get; set; }

      
    }

    public class StoreAds
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AdsId { get; set; }
        public Guid StoreId { get; set; }
        public Guid PromotionId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Status { get; set; }
        public virtual Stores Stores { get; set; }
        [ForeignKey("PromotionId")]
        public virtual PromoteAds PromoteAds { get; set; }


    }

    public class Wallet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid walletId { get; set; }
        public int Balance { get; set; }
        public Guid StoreId { get; set; }

        [ForeignKey("StoreId")]
        public virtual Stores Stores { get; set; }
    }



}
