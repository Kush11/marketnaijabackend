﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models
{
    public class Orders
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OrderId                        { get; set; }
        public Guid CustomerId                     { get; set; }
        public Guid ProductId                      { get; set; }
        public string OrderDate                   { get; set; }
        public string OrderStatus                 { get; set; }
        public Guid storeId                        { get; set; }
      
    }
}
