﻿

using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using PharmaHub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace marketNaija.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserId                          { get; set; }
        public string PhoneNumber                   { get; set; }
        public string Email                         { get; set; }
        public string Address                       { get; set; }
        //public string UserName                      { get; set; }
        public string Token                         { get; set; }
        public byte[] PasswordHash                  { get; set; }
        public byte[] PasswordSalt                  { get; set; }
        public string Password                      { get; set; }
        public string Role                          { get; set; }
        public string ConnectionId                  { get; set; }
        public string FirstName                     { get; set; }
        public string LastName                      { get; set; }
        public string ImageUrl                      { get; set; }
        //public string Language { get; set; }
        public string Gender                        { get; set; }
        public string Age                           { get; set; }
        public string ConnectionPass                { get; set; }



        public virtual ICollection<Review> Review   { get; set; }
        public virtual Customers Customers          { get; set; }
        public virtual Patients Patients            { get; set; }

        public virtual Stores Stores                { get; set; }
    }
     
      

    public class Role
    {
    
        public const string Admin = "Admin";
        public const string User = "User";
        public const string Merchant = "Merchant";
        public const string Customer = "Customer";
        public const string Doctor = "Doctor";
        public const string AdminOrMerchant = Admin + "" + Merchant;
    }
}
