﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Models
{
    public class Prescription
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public Guid PrescriptionId { get; set; }
        public Guid SessionId { get; set; }
        //public string MedicineName { get; set; }
        //public string Dosage { get; set; }
        public string Comment { get; set; }

        [ForeignKey("SessionId")]
        public virtual AppointmentSession AppointmentSession { get; set; }

    }
}
