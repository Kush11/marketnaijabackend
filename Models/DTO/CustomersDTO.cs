﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class CustomersDTO
    {
        public string FirstName  { get; set; }
        public string LastName   { get; set; }
        public string Email      { get; set; }
        public string Address    { get; set; }
        public string Phone      { get; set; }
        public string city       { get; set; }
        public string state      { get; set; }
        public string street     { get; set; }
        public string Zip        { get; set; }
    }
}
