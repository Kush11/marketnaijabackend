﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class StoresDTO
    {
        public int StoreId { get; set; }
        public string StoreName { get; set; }
       
    }
}
