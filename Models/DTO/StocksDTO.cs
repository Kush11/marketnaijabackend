﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class StocksDTO
    {
        public string ProductName { get; set; }
        public int Quantity { get; set; }
    }
}
