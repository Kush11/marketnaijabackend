﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class UserDto
    {
        public Guid Id          { get; set; }
        public string Email    { get; set; }
        public string Address  { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Role     { get; set; }
        public string Token    { get; set; }
    }
}
