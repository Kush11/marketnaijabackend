﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class BrandsDTO
    {
        public string BrandName { get; set; }
        public string Description { get; set; }
    }
}
