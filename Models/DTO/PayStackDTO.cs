﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Models.DTO
{
    public class PayStackDTO
    {
        public string email { get; set; }
        public string amount { get; set; }
    }
}
