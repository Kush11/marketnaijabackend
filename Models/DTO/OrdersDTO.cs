﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class OrdersDTO
    {
        [Key]
        public int OrderId           { get; set; }
        public int CustomerId        { get; set; }
        public string OrderStatus    { get; set; }
        public string ProductId      { get; set; }
        public DateTime OrderDate    { get; set; }
        public DateTime ShippedDate  { get; set; }
    }
}
