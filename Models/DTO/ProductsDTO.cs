﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class ProductsDTO
    {
        public string ProductName { get; set; }
        public int Price          { get; set; }
        public int Discount       { get; set; }
        
    }
}
