﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class MarketsDTO
    {
        public string MarketName { get; set; }
    }
}
