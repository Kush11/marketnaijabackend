﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Models.DTO
{
    public class MerchantDTO
    {
        public string FirstName  { get; set; }
        public string LastName   { get; set; }
        public string phone      { get; set; }
        public string Email      { get; set; }
    }
}
