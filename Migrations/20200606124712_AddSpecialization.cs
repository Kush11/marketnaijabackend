﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class AddSpecialization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Specialization",
                table: "DoctorDetails");

            migrationBuilder.AddColumn<Guid>(
                name: "SpecializationId",
                table: "DoctorDetails",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "DoctorSpecialization",
                columns: table => new
                {
                    SpecializationId = table.Column<Guid>(nullable: false),
                    SpecialiazationName = table.Column<string>(nullable: true),
                    SpecializationCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoctorSpecialization", x => x.SpecializationId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DoctorDetails_SpecializationId",
                table: "DoctorDetails",
                column: "SpecializationId");

            migrationBuilder.AddForeignKey(
                name: "FK_DoctorDetails_DoctorSpecialization_SpecializationId",
                table: "DoctorDetails",
                column: "SpecializationId",
                principalTable: "DoctorSpecialization",
                principalColumn: "SpecializationId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DoctorDetails_DoctorSpecialization_SpecializationId",
                table: "DoctorDetails");

            migrationBuilder.DropTable(
                name: "DoctorSpecialization");

            migrationBuilder.DropIndex(
                name: "IX_DoctorDetails_SpecializationId",
                table: "DoctorDetails");

            migrationBuilder.DropColumn(
                name: "SpecializationId",
                table: "DoctorDetails");

            migrationBuilder.AddColumn<string>(
                name: "Specialization",
                table: "DoctorDetails",
                nullable: true);
        }
    }
}
