﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class updatereview6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "userRate",
                table: "Reviews",
                newName: "UserRate");

            migrationBuilder.RenameColumn(
                name: "userRate",
                table: "DoctorReviews",
                newName: "UserRate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserRate",
                table: "Reviews",
                newName: "userRate");

            migrationBuilder.RenameColumn(
                name: "UserRate",
                table: "DoctorReviews",
                newName: "userRate");
        }
    }
}
