﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class UpdatePres1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dosage",
                table: "Prescriptions");

            migrationBuilder.DropColumn(
                name: "medicineName",
                table: "Prescriptions");

            migrationBuilder.RenameColumn(
                name: "comment",
                table: "Prescriptions",
                newName: "Comment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Comment",
                table: "Prescriptions",
                newName: "comment");

            migrationBuilder.AddColumn<string>(
                name: "dosage",
                table: "Prescriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "medicineName",
                table: "Prescriptions",
                nullable: true);
        }
    }
}
