﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class AddAppointmentSession2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAvailable",
                table: "DoctorDetails",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Prescriptions_SessionId",
                table: "Prescriptions",
                column: "SessionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Prescriptions_AppointmentSessions_SessionId",
                table: "Prescriptions",
                column: "SessionId",
                principalTable: "AppointmentSessions",
                principalColumn: "SessionId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Prescriptions_AppointmentSessions_SessionId",
                table: "Prescriptions");

            migrationBuilder.DropIndex(
                name: "IX_Prescriptions_SessionId",
                table: "Prescriptions");

            migrationBuilder.DropColumn(
                name: "IsAvailable",
                table: "DoctorDetails");
        }
    }
}
