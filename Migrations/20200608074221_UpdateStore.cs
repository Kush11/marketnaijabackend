﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class UpdateStore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stores_Markets_MarketId",
                table: "Stores");

            migrationBuilder.DropIndex(
                name: "IX_Stores_MarketId",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "MarketId",
                table: "Stores");

            migrationBuilder.AddColumn<Guid>(
                name: "MarketsMarketId",
                table: "Stores",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Stores_MarketsMarketId",
                table: "Stores",
                column: "MarketsMarketId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stores_Markets_MarketsMarketId",
                table: "Stores",
                column: "MarketsMarketId",
                principalTable: "Markets",
                principalColumn: "MarketId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stores_Markets_MarketsMarketId",
                table: "Stores");

            migrationBuilder.DropIndex(
                name: "IX_Stores_MarketsMarketId",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "MarketsMarketId",
                table: "Stores");

            migrationBuilder.AddColumn<Guid>(
                name: "MarketId",
                table: "Stores",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Stores_MarketId",
                table: "Stores",
                column: "MarketId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stores_Markets_MarketId",
                table: "Stores",
                column: "MarketId",
                principalTable: "Markets",
                principalColumn: "MarketId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
