﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class updatereview4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DoctorReviews",
                columns: table => new
                {
                    ReviewId = table.Column<Guid>(nullable: false),
                    DoctorId = table.Column<Guid>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    CommentedOn = table.Column<string>(nullable: true),
                    userRate = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoctorReviews", x => x.ReviewId);
                    table.ForeignKey(
                        name: "FK_DoctorReviews_DoctorDetails_DoctorId",
                        column: x => x.DoctorId,
                        principalTable: "DoctorDetails",
                        principalColumn: "DoctorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DoctorReviews_DoctorId",
                table: "DoctorReviews",
                column: "DoctorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DoctorReviews");
        }
    }
}
