﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class updatereview5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "Reviews",
                newName: "CommentBy");

            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "DoctorReviews",
                newName: "CommentBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CommentBy",
                table: "Reviews",
                newName: "UserName");

            migrationBuilder.RenameColumn(
                name: "CommentBy",
                table: "DoctorReviews",
                newName: "UserName");
        }
    }
}
