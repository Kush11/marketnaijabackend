﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class UpddateDoctorDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountNumber",
                table: "DoctorDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DoctorBank",
                table: "DoctorDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentAccountId",
                table: "DoctorDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountNumber",
                table: "DoctorDetails");

            migrationBuilder.DropColumn(
                name: "DoctorBank",
                table: "DoctorDetails");

            migrationBuilder.DropColumn(
                name: "PaymentAccountId",
                table: "DoctorDetails");
        }
    }
}
