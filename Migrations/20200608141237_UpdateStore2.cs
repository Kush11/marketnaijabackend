﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class UpdateStore2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkingStartDateTime",
                table: "DoctorDetails",
                newName: "WorkingDays");

            migrationBuilder.RenameColumn(
                name: "WorkigEndDateTime",
                table: "DoctorDetails",
                newName: "StartTime");

            migrationBuilder.AddColumn<string>(
                name: "EndTime",
                table: "DoctorDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "DoctorDetails");

            migrationBuilder.RenameColumn(
                name: "WorkingDays",
                table: "DoctorDetails",
                newName: "WorkingStartDateTime");

            migrationBuilder.RenameColumn(
                name: "StartTime",
                table: "DoctorDetails",
                newName: "WorkigEndDateTime");
        }
    }
}
