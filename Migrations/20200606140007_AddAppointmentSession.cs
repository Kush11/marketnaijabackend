﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MarketNaijaApp.Migrations
{
    public partial class AddAppointmentSession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Rating",
                table: "DoctorDetails",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AppointmentSessions",
                columns: table => new
                {
                    SessionId = table.Column<Guid>(nullable: false),
                    PatientId = table.Column<Guid>(nullable: false),
                    DoctorId = table.Column<Guid>(nullable: false),
                    SessionStartDateTime = table.Column<string>(nullable: true),
                    SessionEndDateTime = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    SessionCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppointmentSessions", x => x.SessionId);
                    table.ForeignKey(
                        name: "FK_AppointmentSessions_Users_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Prescriptions",
                columns: table => new
                {
                    PrescriptionId = table.Column<Guid>(nullable: false),
                    SessionId = table.Column<Guid>(nullable: false),
                    medicineName = table.Column<string>(nullable: true),
                    dosage = table.Column<string>(nullable: true),
                    comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prescriptions", x => x.PrescriptionId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppointmentSessions_PatientId",
                table: "AppointmentSessions",
                column: "PatientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppointmentSessions");

            migrationBuilder.DropTable(
                name: "Prescriptions");

            migrationBuilder.DropColumn(
                name: "Rating",
                table: "DoctorDetails");
        }
    }
}
