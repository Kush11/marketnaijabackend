﻿

namespace marketNaija.Helpers
{
    public class AppSettings 
    {
        public string Secret { get; set; }
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }


    }
}
