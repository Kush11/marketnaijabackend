﻿using marketNaija.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace marketNaija.Data
{
    public class DummyData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<StoresContext>();
                context.Database.EnsureCreated();

                //if (context.Customers != null)
                //    return;

                //var markets = GetMarkets().ToArray();
                //context.Markets.AddRange(markets);
                //context.SaveChanges();

                //var stores = GetStores(context).ToArray();
                //context.Stores.AddRange(stores);
                //context.SaveChanges();

                //var customers = GetCustomers().ToArray();
                //context.Customers.AddRange(customers);
                //context.SaveChanges();



                //var customerStores = GetCustomerStores(context).ToArray();
                //context.CustomerStores.AddRange(customerStores);
                //context.SaveChanges();







                //var orders = GetOrders(context).ToArray();
                //context.Orders.AddRange(orders);
                //context.SaveChanges();

                //var products = GetProducts(context).ToArray();
                //context.Products.AddRange(products);
                //context.SaveChanges();

                //var orderItem = GetItems(context).ToArray();
                //context.OrderItems.AddRange(orderItem);
                //context.SaveChanges();

                //var role = GetUserRole(context).ToArray();
                //context.UserRole.AddRange(role);
                //context.SaveChanges();

                var user = GetUsers(context).ToArray();
                context.Users.AddRange(user);
                context.SaveChanges();

                //var customerorders = GetCustomerOrders(context).ToArray();
                //context.CustomerOrders.AddRange(customerorders);
                //context.SaveChanges();
            }
        }
        //public static List<Markets> GetMarkets()
        //{
        //    List<Markets> Markets = new List<Markets>()
        //    {
        //        new Markets
        //        {
        //            MarketName = "Gregs Market",
        //            MarketImage = "../../../assets/custom/images/img10.jfif",
        //            Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat nonDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.."
        //        },
        //        new Markets
        //        {
        //            MarketName = "Alaba Market",
        //            MarketImage = "../../../assets/custom/images/img9.jfif",
        //            Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat nonDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.."
        //        },
        //        new Markets
        //        {
        //            MarketName = "Oyingbo Market",
        //            MarketImage = "../../../assets/custom/images/img14.jfif",
        //            Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat nonDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.."
        //        },
        //        new Markets
        //        {
        //            MarketName = "Oyingbo Market",
        //            MarketImage = "../../../assets/custom/images/img8.jfif",
        //            Description = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat nonDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.."
        //        }
        //    };
        //    return Markets;
        //}
        //public static List<Customers> GetCustomers()
        //{
        //    List<Customers> Customers = new List<Customers>() {
        //    new Customers
        //    {

        //        FirstName = "Foo",
        //        LastName = "Bar",
        //        Email = "segunajanaku617@gmail.com",

        //        Address = "olonode",
        //        Phone = "8987463527",
        //        City = "jos",
        //        State = "lagos",
        //        Street = "glover",
        //        Zip = "123",


        //    },
        //    new Customers
        //    {

        //        FirstName = "Greg",
        //        LastName = "jesus",
        //        Email = "segunajanaku617@gmail.com",

        //        Address = "olonode",
        //        Phone = "8987463527",
        //        City = "jos",
        //        State = "lagos",
        //        Street = "glover",
        //        Zip = "123"
        //    },
        //    new Customers
        //    {

        //        FirstName = "Michael",
        //        LastName = "White",
        //        Email = "segunajanaku617@gmail.com",

        //        Address = "olonode",
        //        Phone = "8987463527",
        //        City = "jos",
        //        State = "lagos",
        //        Street = "glover",
        //        Zip = "123"
        //    },
        //    new Customers
        //    {

        //        FirstName = "kelly",
        //        LastName = "Greyjoy",
        //        Email = "segunajanaku617@gmail.com",

        //        Address = "olonode",
        //        Phone = "8987463527",
        //         City = "jos",
        //        State = "lagos",
        //        Street = "glover",
        //        Zip = "123"
        //    },
        //    new Customers
        //    {

        //        FirstName = "Richard",
        //        LastName = "Bruce",
        //        Email = "segunajanaku617@gmail.com",

        //        Address = "olonode",
        //        Phone = "8987463527",
        //        City = "jos",
        //        State = "lagos",
        //        Street = "glover",
        //        Zip = "123"
        //    }
        //    };

        //    return Customers;
        //}

        //public static List<Merchants> GetMerchants()
        //{
        //    List<Merchants> Merchants = new List<Merchants>()
        //    {
        //        new Merchants
        //        {
        //            FirstName = "John",
        //            LastName = "Wick",
        //            Email = "segunajanaku617@gmail.com",

        //            Phone = "8987463527",
        //        },
        //        new Merchants
        //        {

        //            FirstName = "Mark",
        //            LastName = "Pellicore",
        //            Email = "segunajanaku617@gmail.com",

        //            Phone = "8987463527",

        //        },
        //        new Merchants
        //        {

        //            FirstName = "susan",
        //            LastName = "crey",
        //            Email = "segunajanaku617@gmail.com",

        //            Phone = "8987463527",

        //        },
        //        new Merchants
        //        {

        //            FirstName = "chris",
        //            LastName = "arthur",
        //            Email = "segunajanaku617@gmail.com",

        //            Phone = "8987463527",

        //        },
        //        new Merchants
        //        {

        //            FirstName = "evans",
        //            LastName = "allmighty",
        //            Email = "segunajanaku617@gmail.com",

        //            Phone = "8987463527",

        //        },
        //        new Merchants
        //        {

        //            FirstName = "joy",
        //            LastName = "cook",
        //            Email = "segunajanaku617@gmail.com",

        //            Phone = "8987463527",

        //        }
        //    };
        //    return Merchants;
        //}
        //public static List<Stores> GetStores(StoresContext db)
        //{
        //    List<Stores> Stores = new List<Stores>()
        //    {
        //        new Stores
        //        {

        //            StoreName = "Shade's Stores",
        //            Email = "storefront@gmail.com",
        //            FirstName = "Shade",
        //            LastName = "Oladapo",
        //            MarketId = 1,
        //            PhoneNumber = "08076474657",
        //            ConfirmStatus = true

        //        },
        //        new Stores
        //        {

        //            StoreName = "Shade's Stores",
        //            Email = "storefront@gmail.com",
        //            FirstName = "Shade",
        //            LastName = "Oladapo",
        //            MarketId = 2,
        //            PhoneNumber = "08076474657"


        //        },
        //        new Stores
        //        {

        //             StoreName = "Shade's Stores",
        //            Email = "storefront@gmail.com",
        //            FirstName = "Shade",
        //            LastName = "Oladapo",
        //            MarketId = 2,
        //            PhoneNumber = "08076474657"

        //        },
        //        new Stores
        //        {

        //            StoreName = "Shade's Stores",
        //            Email = "storefront@gmail.com",
        //            FirstName = "Shade",
        //            LastName = "Oladapo",
        //            MarketId = 2,
        //            PhoneNumber = "08076474657"

        //        },
        //        new Stores
        //        {

        //             StoreName = "Shade's Stores",
        //            Email = "storefront@gmail.com",
        //            FirstName = "Shade",
        //            LastName = "Oladapo",
        //            MarketId = 3,
        //            PhoneNumber = "08076474657"

        //        },
        //        new Stores
        //        {

        //            StoreName = "Shade's Stores",
        //            Email = "storefront@gmail.com",
        //            FirstName = "Shade",
        //            LastName = "Oladapo",
        //            MarketId = 3,
        //            PhoneNumber = "08076474657"

        //        },
        //        new Stores
        //        {

        //             StoreName = "Shade's Stores",
        //            Email = "storefront@gmail.com",
        //            FirstName = "Shade",
        //            LastName = "Oladapo",
        //            MarketId = 5,
        //            PhoneNumber = "08076474657"

        //        }
        //    };
        //    return Stores;
        //}


        //public static List<CustomerStores> GetCustomerStores(StoresContext db)
        //{
        //    List<CustomerStores> CustomerStores = new List<CustomerStores>()
        //    {
        //        new CustomerStores
        //        {
        //            StoreId = 1,
        //            CustomerId = 2
        //        },
        //        new CustomerStores
        //        {
        //            StoreId = 1,
        //            CustomerId = 3
        //        },
        //        new CustomerStores
        //        {
        //            StoreId = 6,
        //            CustomerId = 1
        //        },
        //        new CustomerStores
        //        {
        //            StoreId = 2,
        //            CustomerId = 4
        //        },
        //        new CustomerStores
        //        {
        //            StoreId = 5,
        //            CustomerId = 5
        //        },
        //         new CustomerStores
        //        {
        //            StoreId = 5,
        //            CustomerId = 1
        //        }
        //    };
        //    return CustomerStores;
        //}

        //public static List<Products> GetProducts(StoresContext db)
        //{
        //    List<Products> Products = new List<Products>()
        //    {
        //        new Products
        //        {

        //            ProductName = "Binatone Steel Iron",
        //            Price = 20,
        //            Discount = 5,
        //            StoreId = 2,


        //        },
        //        new Products
        //        {

        //            ProductName = "LG Plasma TV",
        //            Price = 200,
        //            Discount = 15,
        //            StoreId = 1,


        //        },
        //        new Products
        //        {

        //            ProductName = "LG Plasma TV",
        //            Price = 200,
        //            Discount = 15,
        //            StoreId = 1,


        //        },
        //        new Products
        //        {

        //            ProductName = "LG Plasma Screen",
        //            Price = 200,
        //            Discount = 15,
        //            StoreId = 3,

        //        },
        //        new Products
        //        {

        //            ProductName = "LG Plasma Screen",
        //            Price = 200,
        //            Discount = 15,
        //            StoreId = 2,

        //        },
        //        new Products
        //        {

        //            ProductName = "LG Plasma Screen",
        //            Price = 200,
        //            Discount = 15,
        //            StoreId = 4,

        //        }
        //    };
        //    return Products;
        //}





        //public static List<Orders> GetOrders(StoresContext db)
        //{
        //    List<Orders> Orders = new List<Orders>()
        //    {
        //        new Orders
        //        {
        //            CustomerId = 3,
        //            OrderDate = new DateTime().Date,
        //            ShippedDate = new DateTime().Date,
        //            OrderStatus = 1,
        //            ProductId = 1

        //        },
        //        new Orders
        //        {
        //            CustomerId = 4,
        //            OrderDate = new DateTime().Date,
        //            ShippedDate = new DateTime().Date,
        //            OrderStatus = 1,
        //            ProductId = 1

        //        },
        //        new Orders
        //        {
        //            CustomerId = 3,
        //            OrderDate = new DateTime().Date,
        //            ShippedDate = new DateTime().Date,
        //            OrderStatus = 1,
        //            ProductId = 2

        //        },
        //        new Orders
        //        {
        //            CustomerId = 1,
        //            OrderDate = new DateTime().Date,
        //            ShippedDate = new DateTime().Date,
        //            OrderStatus = 3,
        //            ProductId = 2

        //        },
        //        new Orders
        //        {
        //            CustomerId = 1,
        //            OrderDate = new DateTime().Date,
        //            ShippedDate = new DateTime().Date,
        //            OrderStatus = 2,
        //            ProductId = 4


        //        }

        //    };
        //    return Orders;
        //}

        //public static List<OrderItems> GetItems(StoresContext db)
        //{
        //    List<OrderItems> Item = new List<OrderItems>()
        //    {
        //        new OrderItems
        //        {
        //            OrderId = 3,

        //            SubTotalPrice = 450,
        //            Discount = 5,
        //            TotalPrice = 22.5,
        //            Quantity = 1,
        //            StoreId = 1
        //        },
        //        new OrderItems
        //        {
        //            OrderId = 3,

        //            SubTotalPrice = 450,
        //            Discount = 5,
        //            TotalPrice = 22.5,
        //            Quantity = 1,
        //            StoreId = 2
        //        },
        //        new OrderItems
        //        {
        //            OrderId = 2,

        //            SubTotalPrice = 450,
        //            Discount = 5,
        //            TotalPrice = 22.5,
        //            Quantity = 1,
        //            StoreId = 2
        //        },
        //        new OrderItems
        //        {
        //            OrderId = 1,

        //            SubTotalPrice = 450,
        //            Discount = 5,
        //            TotalPrice = 22.5,
        //            Quantity = 5,
        //            StoreId = 3
        //        }
        //    };
        //    return Item;
        //}
        public static List<User> GetUsers(StoresContext db)
        {
            List<User> Users = new List<User>()
            {
                new User
                {
                    Password = "admin",
                    Email = "admin@gmail.com",
                    Address = "No 4 admiralty way",
                    PhoneNumber = "0908763546535",
                    Role = Role.Admin
                },
                //new User
                //{

                //    Username = "king",
                //    Password = "helloworld",
                //    Email = "segun@gmail.com",
                //    Address = "No 4 admiralty way",

                //    Role = Role.Customer
                //},
                //new User
                //{
                //    Username = "king",
                //    Password = "helloworld",
                //    Email = "segun@gmail.com",
                //    Address = "No 4 admiralty way",

                //    Role =  Role.Merchant
                //}
            };
            return Users;
        }

        //public static List<UserRole> GetUserRole(StoresContext db)
        //{
        //    List<UserRole> Role = new List<UserRole>()
        //    {
        //        new UserRole
        //        {

        //           Role = "admin"
        //        },

        //        new UserRole
        //        {
        //           Role = "merchant"
        //        },
        //        new UserRole
        //        {

        //           Role = "customer"
        //        },
        //        new UserRole
        //        {

        //           Role = "user"
        //        }
        //    };
        //    return Role;
        //}

        //public static List<CustomerOrders> GetCustomerOrders(StoresContext db)
        //{
        //    List<CustomerOrders> CustomerOrders = new List<CustomerOrders>()
        //    {
        //        new CustomerOrders
        //        {
        //             OrderId = 2,
        //            CustomerId = 1

        //        },
        //        new CustomerOrders
        //        {
        //            OrderId = 1,
        //            CustomerId = 3
        //        },
        //        new CustomerOrders
        //        {
        //            OrderId = 6,
        //            CustomerId = 1
        //        },
        //        new CustomerOrders
        //        {
        //            OrderId = 2,
        //            CustomerId = 4
        //        },
        //        new CustomerOrders
        //        {
        //            OrderId = 5,
        //            CustomerId = 5
        //        },
        //         new CustomerOrders
        //        {
        //            OrderId = 5,
        //            CustomerId = 1
        //        }
        //    };
        //    return CustomerOrders;
        //}
    }
}
