﻿using marketNaija.Models;
using marketNaija.Models.DTO;
using MarketNaijaApp.Models;
using Microsoft.EntityFrameworkCore;
using PharmaHub.Models;

namespace marketNaija.Data
{
    public class StoresContext : DbContext
    {
       

        public StoresContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Entity<CustomerStores>()
            //    .HasKey(cs => new { cs.CustomerId, cs.StoreId });
            //builder.Entity<Customers>()
            //    .HasOne(c => c.Stores);
            //builder.Entity<Stores>()
            //    .HasMany(cs => cs.Customers);

          

            //builder.Entity<OrderItems>()
            //    .HasOne(p => p.Stores)
            //    .WithMany(c => c.Orders)
            //    .HasForeignKey(s => s.storeId)
            //    .OnDelete(DeleteBehavior.Cascade);

        }
        public DbSet<Markets> Markets { get; set; }
        public DbSet<Customers> Customers { get; set; }
     
        public DbSet<Products> Products { get; set; }
        public DbSet<Orders> Orders { get; set; }
      
        public DbSet<Stores> Stores { get; set; }
        //public DbSet<CustomerStores> CustomerStores { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<file> Image { get; set; }
        public DbSet<StoreImage> StoreImage { get; set; }
        public DbSet<StoreAds> StoreAds { get; set; }
        public DbSet<ProductAds>ProductAds { get; set; }
        public DbSet<Wallet>wallet { get; set; }
        public DbSet<PromoteAds> PromoteAds { get; set; }
        public DbSet<Hero> Hero { get; set; }
        public DbSet<MarketImage> MarketImage { get; set; }
        public DbSet<DoctorDetails> DoctorDetails { get; set; }
        public DbSet<DoctorSpecialization> DoctorSpecialization { get; set; }
        public DbSet<AppointmentSession> AppointmentSessions { get; set; }
        public DbSet<Prescription> Prescriptions { get; set; }
        public DbSet<DoctorReview> DoctorReviews { get; set; }
        public DbSet<Patients> Patients { get; set; }
        public DbSet<PaymentData> PaymentData { get; set; }




    }
}
