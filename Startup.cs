﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using marketNaija.Data;
using marketNaija.Extensions;
using marketNaija.Helpers;
using marketNaija.Models;
using marketNaija.Models.DTO;
using marketNaija.Repository;
using marketNaija.Repository.Interface;
using marketNaija.Services;
using MarketNaijaApp.Models;
using MarketNaijaApp.Repository;
using MarketNaijaApp.Repository.Interface;
using MarketNaijaApp.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PharmaHub.Models;
using PharmaHub.Repository;
using PharmaHub.Services;

namespace marketNaija
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<StoresContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddMvc().AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddScoped<_dataRepository<Customers>, CustomerRepository>();
            services.AddScoped<_dataRepository<Stores>, StoreRepository>();
            services.AddScoped<_dataRepository<Orders>, OrdersRepository>();
            services.AddScoped<_dataRepository<Products>, ProductsRepository>();
            services.AddScoped<_dataRepository<Review>, ReviewRepository>();
            services.AddScoped<_imageRepository<StoreImage>, StoreImageRepository>();
            services.AddScoped<_dataRepository<Wallet>, WalletRepository>();
            services.AddScoped<_dataRepository<DoctorDetails>, DoctorDetailRepository>();
            services.AddScoped<_dataRepository<DoctorSpecialization>, DoctorSpecializationRepository>();
            services.AddScoped<_dataRepository<AppointmentSession>, AppointmentSessionRepository>();
            services.AddScoped<_dataRepository<Prescription>, PrescriptionRepository>();
            services.AddScoped<_dataRepository<DoctorReview>, DoctorReviewRepository>();
            services.AddScoped<_dataRepository<Patients>, PatientRepository>();




            services.AddScoped<_dataRepository<StoreAds>, StoreAdsRepository>();
            services.AddScoped<_dataRepository<ProductAds>, ProductsAdsRepository>();
            services.AddScoped<_dataRepository<PromoteAds>, PromoteAdsRepository>();
            services.AddScoped<IPaymentDataEncryption, PaymentDataEncryptionService>();



            //services.AddScoped<IDataRepository<User>, UserRepository>();
            // configure DI for application services
            services.AddScoped<IUserService, UserService>();
            services.AddSwaggerDocumentation();

            services.ConfigureCors();
            services.ConfigureIISIntegration();
            //services.ConfigureServices();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.Configure<SendEmail>(Configuration.GetSection("EmailSettings"));
            //services.AddAutoMapper();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDto, User>();
                cfg.CreateMap<StoresDTO, Stores>();
                cfg.CreateMap<ProductsDTO, Products>();
            });

            IMapper mapper = config.CreateMapper();

            services.AddSingleton(mapper);
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        
            services.AddMemoryCache();
            services.AddSession();
            services.AddSingleton<IEmailService, EmailSender>();
            //services.AddAutoMapper();



            // configure strongly typed settings object
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

                x.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];


                        //if request is for hub

                        var path = context.HttpContext.Request.Path;
                        if (!String.IsNullOrEmpty(accessToken) && (path.StartsWithSegments("/notifyHub")))
                        {
                            //Read the token out of the query string
                            context.Token = accessToken;
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSignalR();
            services.AddSingleton<IUserIdProvider, UserIdProvider>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.Use(async (context, next) =>
                {
                    await next();
                    if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                    {
                        context.Request.Path = "/index.html";
                        await next();
                    }
                });
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseCors("CorsPolicy");
            app.UseSignalR(routes =>
            {
                routes.MapHub<NotifyHub>("/notifyHub");
            });
            app.UseStaticFiles();
            app.UseSession();
         
            app.UseMvc();
            app.UseSwaggerDocumentation();

            //DummyData.Initialize(app);
        }
    }
}

