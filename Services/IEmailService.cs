﻿using MailKit.Net.Smtp;
using MarketNaijaApp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketNaijaApp.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }

    public class EmailSender : IEmailService
    {
        private readonly SendEmail _sendEmail;
        private readonly IHostingEnvironment _env;

        public EmailSender(IOptions<SendEmail> sendEmail, IHostingEnvironment env)
        {
            _sendEmail = sendEmail.Value;
            _env = env;
        }
        public async  Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(_sendEmail.SenderName, _sendEmail.Sender));
                mimeMessage.To.Add(new MailboxAddress(email));
                mimeMessage.Subject = subject;
                mimeMessage.Body = new TextPart("html")
                {
                    Text = message
                };

                using(var client  = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    if (_env.IsDevelopment())
                    {
                        // The third parameter is useSSL (true if the client should make an SSL-wrapped 
                        // connection to the server; otherwise, false )
                        await client.ConnectAsync(_sendEmail.MailServer, _sendEmail.MailPort, true);
                    }
                    else
                    {
                        await client.ConnectAsync(_sendEmail.MailServer);
                    }
                    // Note: only needed if the SMTP server requires authentication
                    await client.AuthenticateAsync(_sendEmail.Sender, _sendEmail.Password);
                    await client.SendAsync(mimeMessage);
                    await client.DisconnectAsync(true);
                }
            }
            catch( Exception ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
           
        }
    }
}
